#ifndef TEST_H
#define TEST_H

#include <ostream>

namespace test {

class Test {
public:
    virtual bool performTest(std::ostream &out) = 0;
};

}

#endif // TEST_H
