#ifndef INTERPRETERTEST_H
#define INTERPRETERTEST_H

#include "../src/interpreter.h"
#include "../src/frontend/parser.h"
#include <sstream>


class InterpreterTest
{
protected:
    std::ostream &out;
    MSAllocator * alloca;
    int total;
    int passed;

    RValue * run(std::string query) {
        out << "Interpreting: " << query << "\n";
        RValue * res = nullptr;
        std::istringstream is(query);
        try {
            ast::Function *program = Parser::parseInput(is);
            res = interpreter::Interpreter::interpret(program->body, alloca->getTopEnvironment(), alloca);
            delete program;
        } catch (ScannerException se) {
            out << "Scanner error: " << se.what();
        } catch (ParserException pe) {
            out << "Parser error: " << pe.what();
        } catch (InterpreterException ie) {
            out << "Interpreter error: " << ie.what();
        }

        return res;
    }

    template <typename T>
    void dumpVector(std::vector<T> &data) {
        out << "[";
        for (T val : data)
            out << val << " ";
        out << "]";
    }

public:
    InterpreterTest(std::ostream &output, std::string testName): out(output),
        total(0), passed(0) {
        alloca = new MSAllocator(4000);

        out << "TEST: " << testName << "\n";
        out << "------------------------------\n";
    }

    ~InterpreterTest() {
        if (alloca != nullptr)
            delete alloca;
    }

    InterpreterTest & loadOnly(std::string query) {
        total++;
        RValue * res = run(query);
        if (res != nullptr) {
            passed++;
            out << "OK: Successfully interpreted.\n";
        } else {
            out << "FAIL: Not able to interpret.\n";
        }
        return *this;
    }

    InterpreterTest & testDouble(std::string query, double expected) {
        total++;
        RValue * result = run(query);
        if (result != nullptr) {
            try {
                if (result->type == RValueType::Double && result->dblVector.size == 1
                    && result->dblVector.data[0] == expected) {
                    passed++;
                    out << "OK: expected and got " << expected << " (double) \n";
                } else {
                    out << "FAIL: got " << result->dblVector.data[0] << ", expected "
                        << expected << "\n";
                }
            } catch (std::exception e) {
                out << "Runtime error: got this: " << e.what();
            }
        }
        return *this;
    }

#define STREQ(a, b) (strcmp(a,b) == 0)
    InterpreterTest & testCharacter(std::string query, const char * expected) {
        total++;
        RValue * result = run(query);
        if (result != nullptr) {
            try {
                if (result->type == RValueType::Character && result->charVector.size == strlen(expected)
                    && STREQ(expected, result->charVector.data)) {
                    passed++;
                    out << "OK: expected and got " << expected << " (character) \n";
                } else {
                    out << "FAIL: got " << result->charVector.data << ", expected "
                        << expected << "\n";
                }
            } catch (std::exception e) {
                out << "Runtime error: got this: " << e.what();
            }
        }
        return *this;
    }

    InterpreterTest & testInteger(std::string query, int expected) {
        total++;
        RValue * result = run(query);
        if (result != nullptr) {
            try {
                if (result->type == RValueType::Integer && result->intVector.size == 1
                    && result->intVector.data[0] == expected) {
                    passed++;
                    out << "OK: expected and got " << expected << " (integer) \n";
                } else {
                    out << "FAIL: got " << result->intVector.data[0] << ", expected "
                        << expected << "\n";
                }
            } catch (std::exception e) {
                out << "Runtime error: got this: " << e.what();
            }
        }
        return *this;
    }



    InterpreterTest & summary() {
        out << "***********************************\n";
        out << "SCORE: " << passed << " / " << total << "\n";
        out << "***********************************\n";
        return *this;
    }

    InterpreterTest & dumpHeap() {
        alloca->dumpHeap(out);
        return *this;
    }

    static void test(std::ostream &output);
};

#endif // INTERPRETERTEST_H
