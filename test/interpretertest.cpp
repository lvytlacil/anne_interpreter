#include "interpretertest.h"

static std::ostream * storedOut;

static void simpleArithmetics() {
    InterpreterTest testCase(*storedOut, "Simple Arithmetics I");
    testCase.testInteger("3 + 5", 8)
            .testInteger("12 - 10", 2)
            .testInteger("9 * 8", 72)
            .testInteger("25 / 6", 4)
            .testDouble("11.0 / 2", 5.5)
            .testCharacter("\"Improved \" + \"Overpower\"", "Improved Overpower")
            .summary();
}

static void indexing() {
    InterpreterTest testCase(*storedOut, "Index accesing");
    testCase.loadOnly("x = array(4,12,20,11,99,9999,9809)")
            .testInteger("x[0]", 4)
            .testInteger("x[1]", 12)
            .testInteger("x[2]", 20)
            .testInteger("x[3]+x[4]", 110)
            .summary();
}

static void functions() {
    InterpreterTest * testCase = new InterpreterTest(*storedOut, "Nested Environments");
    testCase->loadOnly("a = 12 x = 5 fun = function() {x = 16 x}")
            .testInteger("x", 5)
            .testInteger("fun()", 16)
            .testInteger("fun() x", 5)
            .testInteger("x + fun()", 21)
            .loadOnly("x = 4000")
            .testInteger("fun()", 16)
            .testInteger("x + fun()", 4016)
            .testInteger("a", 12)
            .testInteger("bar = function() {a} bar()", 12)
            .summary();

    delete testCase;

    testCase = new InterpreterTest(*storedOut, "Deeper nesting");
    testCase->loadOnly("a = 4 foo = function() {b = 15 bar = function() {a + b} bar()}")
            .testInteger("foo()", 19)
            .testInteger("baz = function() {x = function() { 4 * foo() } x() } baz()", 76)
            .summary();
    delete testCase;

    testCase = new InterpreterTest(*storedOut, "Function arguments");
    testCase->loadOnly("x = function(a,b,c) {a + b + c}")
            .testInteger("x(1,2,3)", 6)
            .testInteger("arg1 = 10 arg2 = 5 x(arg1, arg2, 1)", 16)
            .testInteger("a = 2 b = 4 c = 6 x(a,b,c)", 12)
            .testInteger("x(a + b + c, 4 * b, c / c)", 29)
            .testInteger("x(x(1,2,3), x(2,2,2), x(a,b,c))", 24)
            .summary();
    delete testCase;

    testCase = new InterpreterTest(*storedOut, "Example computation: Triangle numbers");
    testCase->loadOnly("tri = function(n) {\nr = 0 i = 0\nwhile (i < n) {r = r + i i = i + 1} r }")
            .testInteger("tri(1)", 0)
            .testInteger("tri(2)", 1)
            .testInteger("tri(3)", 3)
            .testInteger("tri(4)", 6)
            .testInteger("tri(5)", 10)
            .testInteger("tri(tri(4))", 15)
            .summary();
    delete testCase;

    testCase = new InterpreterTest(*storedOut, "Functions as first-class values");
    testCase->loadOnly("x = function() { t = 10 nest = function(p) {t + p} nest}")
            .testInteger("y = x() y(500)", 510)
            .loadOnly("z = function(a, b) { function(c) {a + b + c} }");
            testCase->testDouble("w = z(1.1, 10.1) w(100.1)", 111.3)
            .summary();
    delete testCase;
}

void indexAssignments() {
    InterpreterTest * testCase = new InterpreterTest(*storedOut, "Mutable Index assignments I");
    testCase->loadOnly("a = array(1,2,3,4) b = array(1.0, 2.0, 3.0, 4.0) c = \"Fireball\"")
            .testInteger("a[0] = 30 a[0]", 30)
            .testInteger("a[1]", 2)
            .testInteger("a[2]", 3)
            .testInteger("a[3]", 4)
            .testDouble("b[2] = 500 b[0]", 1.0)
            .testDouble("b[1]", 2.0)
            .testDouble("b[2] + b[3]", 504.0)
            .testCharacter("c[array(4,5,6,7)] = \"spin\" c", "Firespin")
            .testCharacter("c[0] = \"Molten Armor\" c", "Mirespin")
            .summary();
    delete testCase;
}

void assignments() {
    InterpreterTest testCase(*storedOut, "Assignments");
    testCase.testInteger("a = 12", 12)
    .testInteger("b = a", 12)
    .testInteger("a = 2 b", 12)
    .testInteger("a", 2)
    .summary();



}

void core() {
    InterpreterTest testCase(*storedOut, "Assignments");
    testCase.loadOnly("a = 4 foo = function() {b = 15 bar = function() {a + b} bar()}")
            .testInteger("foo()", 19)
            //.testInteger("baz = function() {x = function() { 4 * foo() } x() } baz()", 76)
            .summary();

}

void InterpreterTest::test(std::ostream &output) {
    storedOut = &output;
    //core();
    simpleArithmetics();
    indexing();
    functions();
    assignments();
    indexAssignments();
}
