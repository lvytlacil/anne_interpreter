#include "../src/frontend/scanner.h"
#include "test.h"
#include <vector>
#include <string>
#include <sstream>

namespace test {

class ScannerTest : public Test {
    std::string inputStr = "function (a,b) { if (a) b }";
public:

    bool performTest(std::ostream &out) override {
        std::vector<Token> tokens;

        std::istringstream input(inputStr);

        Scanner scanner(input);
        Token next(Token::Type::eof);
        do {
            next = scanner.getNextToken();
            tokens.push_back(next);
        } while (next.type != Token::Type::eof);

        for (Token token : tokens) {
            out << Token::typeAsString(token.type) << " ";
        }

        return true;
    }
};







}
