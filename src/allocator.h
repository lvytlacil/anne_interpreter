#ifndef ALLOCATOR_H
#define ALLOCATOR_H


#include <cstdint>
#include <stack>
#include "runtime.h"
#include "environment.h"

class SymbolTable;

class Allocator
{
public:

    virtual RValue * allocate(uint32_t requestedBytes) = 0;
    virtual RValue * makeIntegerVector(int value) = 0;
    virtual RValue * makeIntegerVector(int * data, int size) = 0;
    virtual RValue * makeIntegerVectorS(int size) = 0;
    virtual RValue * makeDoubleVector(double value) = 0;
    virtual RValue * makeDoubleVector(double * data, int size) = 0;
    virtual RValue * makeDoubleVectorS(int size) = 0;
    virtual RValue * makeCharacterVectorS(int size) = 0;
    virtual RValue * makeCharacterVector(const char *str) = 0;
    virtual RValue * makeEnvironmentHolder(Environment *env) = 0;
    virtual RValue * makeLocalEnvironment(RValue *parent) = 0;
    virtual RValue * makeFunctionDefinition(ast::Function *functionCode, RValue *envHolder, int argSize) = 0;
    virtual RValue * getTopEnvironment() = 0;
    virtual RValue * getNilSingletion() = 0;
    virtual bool isSingleton(RValue *val) = 0;
    virtual void setEnvironmentRoot(RValue * env) = 0;
    virtual void setTemporaryStack(std::vector<RValue*> &st) = 0;

    virtual RValue * makeOrGetCharacterVector(const char *str) = 0;


    Allocator() { }

    virtual ~Allocator() { }
};

class MSAllocator : public Allocator {
private:
    struct AlignStruct {
        uint32_t a,b,c;
        /* 8 for possible padding in the union, 4 for the rest of the members.
        Total 12 = 3 * sizeof(uint32_t)*/
    };

    /* Determines the smallest memory allocation unit. It should be a type bigger than the alignment in RValue.
    If you want to play super safe, define block as RValue itself (but thats costly in terms of allocated object sizes). */
    typedef AlignStruct block;

    /* Stores an info about a chunk, that the OS have allocated for our to perform our own memory management */
    struct ChunkData {
        block * chunkStart; // The block base address
        block * chunkEnd;
        block * nextFastAlloca; // Address for next fast allocation
        RValue * freeListHead; // Head of the free block list.
    };

    static const uint32_t INT_VECTOR_BYTES;
    static const uint32_t DOUBLE_VECTOR_BYTES;
    static const uint32_t CHAR_VECTOR_BYTES ;
    static const uint32_t ENVIRONMENT_HOLDER_BYTES ;
    static const uint32_t FUNCTION_DEFINITION_BYTES ;
    static const uint32_t NIL_BYTES;
    static const uint32_t FREE_BLOCK_BYTES;
    static const uint32_t MIN_FREEBLOCK_SIZE_IN_BLOCKS;

    const static int SINGLETON_COUNT = 3;

    /* The roots */
    // 0..NIL 1..int(0) 2..int(1)
    RValue * singletons_[SINGLETON_COUNT];
    RValue * envRoot_; // The environment root
    RValue * topEnv_; // The top environment
    std::vector<RValue*> *temporaries;

    SymbolTable * symbolTable_; // Symbol table for caching character vectors
    ChunkData chunks_[1]; // One chunk for this implementation;


    static uint32_t padByteSizeAndConvertToBlockSize(size_t sizeInBytes);

    /**
     * @brief First-fit strategy on free blocks to find a suitable free memory
     * @param sizeInBlocks
     * @param chunk
     * @return
     */
    RValue * allocateFromFreelist(uint32_t sizeInBlocks, ChunkData &chunk);

    inline bool isFree(RValue *block) {
        return block->type == RValueType::FreeBlock;
    }

    /**
     * @brief Create a singleton and stores it into singletons_[0]. Do not call this more than once per Allocator.
     */
    void createNilSingleton();
    void createSmallIntSingletions();

    RValue * allocateInChunk(uint32_t requestedBytes, ChunkData &chunk);

    static void markKeyValuePair(RValue * key, RValue *val);

    void markGenericValue(RValue *val);
    void mark();    
    void sweep();
    inline void sweepValue(RValue *val);

    void makeTopEnvironment();


public:
    MSAllocator(uint32_t sizeInBytes);
    ~MSAllocator() override;

    RValue * makeIntegerVector(int value) override;
    RValue * makeIntegerVector(int * data, int size) override;
    RValue * makeIntegerVectorS(int size) override;
    RValue * makeDoubleVector(double value) override;
    RValue * makeDoubleVector(double * data, int size) override;
    RValue * makeDoubleVectorS(int size) override;
    RValue * makeCharacterVectorS(int size) override;
    RValue * makeCharacterVector(const char *str) override;
    RValue * makeEnvironmentHolder(Environment *env) override;
    RValue * makeLocalEnvironment(RValue *parentHolder) override;
    RValue * makeFunctionDefinition(ast::Function *functionCode, RValue *envHolder, int argsSize) override;
    RValue * makeOrGetCharacterVector(const char *str) override;
    RValue * getNilSingletion() override;
    void setEnvironmentRoot(RValue * env) override;
    void setTemporaryStack(std::vector<RValue*> &st) {
        temporaries = &st;
    }
    RValue * getTopEnvironment() override {
        return topEnv_;
    }

    RValue * allocate(uint32_t requestedBytes) override {
        return allocateInChunk(requestedBytes, chunks_[0]);
    }

    void dumpHeap(std::ostream &out) {
        out << "WALKING THROUGH THE HEAP:\n";
        block *start = chunks_[0].chunkStart;
        int i = 1;
        while (start < chunks_[0].nextFastAlloca) {
            RValue * obj = (RValue*) start;//reinterpret_cast<RValue*>(start);
            if (!isFree(obj)) {
                out << "Object no. " << i << " at " << start << "\n";
                out << "Size in blocks: " << obj->sizeTotal << ", type: ";
                RValue::dump(obj, out);
                out << ", marked: " << obj->marked << "\n";               
                ++i;
                start += obj->sizeTotal;
            } else {
                out << "Free block at " << start << "\n";
                out << "Size in blocks: " << obj->sizeTotal << "\n";
                start += obj->sizeTotal;
            }
            out << "-------------\n";
        }
        out << "\n";
        dumpListOfFreeBlocks(out);
    }

    void dumpListOfFreeBlocks(std::ostream &out) {
        out << "LIST OF FREE BLOCKS:\n";
        RValue * act = chunks_[0].freeListHead;
        int i = 1;
        while (act != nullptr) {
            out << "Freeblock no. " << i << " at " << act << "\n";
            out << "Size in blocks: " << act->sizeTotal << "\n";
            out << "------------------\n";
            act = act->freeBlock.next;
            i++;
        }
        out << "\n";
    }

    virtual bool isSingleton(RValue *val) override {
        for (int i = 0; i < SINGLETON_COUNT; i++)
            if (val == singletons_[i])
                return true;
        return false;
    }

    void testSymbolTable(std::ostream &out);

};

/*-------------- SYMBOL TABLE -------------------------*/



#endif // ALLOCATOR_H
