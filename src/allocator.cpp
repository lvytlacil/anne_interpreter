#include <iostream>
#include "allocator.h"
#include "symboltable.h"
#include "util.h"

#ifdef DEBUG
#define ASSERT(arg) if (!(arg)) throw InterpreterException("Assertion error during allocator's work");
#define ASSERTMSG(arg, msg) if (!(arg)) throw msg;
#else
#define ASSERT(arg)
#define ASSERTMSG(arg, msg)
#endif


uint32_t MSAllocator::padByteSizeAndConvertToBlockSize(size_t sizeInBytes) {
    return sizeInBytes / sizeof(block) + (sizeInBytes % sizeof(block) != 0);
}

const uint32_t MSAllocator::INT_VECTOR_BYTES = sizeof(RValue::type) + sizeof(RValue::marked) +
                            sizeof(RValue::sizeTotal) + sizeof(RValue::intVector) + sizeof(RValue::shareLinks);
const uint32_t MSAllocator::DOUBLE_VECTOR_BYTES = sizeof(RValue::type) + sizeof(RValue::marked) +
                            sizeof(RValue::sizeTotal) + sizeof(RValue::dblVector) + sizeof(RValue::shareLinks);
const uint32_t MSAllocator::CHAR_VECTOR_BYTES = sizeof(RValue::type) + sizeof(RValue::marked) +
                            sizeof(RValue::sizeTotal) + sizeof(RValue::charVector) + sizeof(RValue::shareLinks);
const uint32_t MSAllocator::ENVIRONMENT_HOLDER_BYTES = sizeof(RValue::type) + sizeof(RValue::marked) +
                            sizeof(RValue::sizeTotal) + sizeof(RValue::environment) + sizeof(RValue::shareLinks);
const uint32_t MSAllocator::FUNCTION_DEFINITION_BYTES = sizeof(RValue::type) + sizeof(RValue::marked) +
                            sizeof(RValue::sizeTotal) + sizeof(RValue::funcDef) + sizeof(RValue::shareLinks);
const uint32_t MSAllocator::NIL_BYTES = sizeof(RValue::type) + sizeof(RValue::marked) + sizeof(RValue::sizeTotal)
                                + sizeof(RValue::shareLinks);

const uint32_t MSAllocator::FREE_BLOCK_BYTES = sizeof(RValue::type) + sizeof(RValue::marked) +
                            sizeof(RValue::sizeTotal) + sizeof(RValue::freeBlock) + + sizeof(RValue::shareLinks);
const uint32_t MSAllocator::MIN_FREEBLOCK_SIZE_IN_BLOCKS = MSAllocator::padByteSizeAndConvertToBlockSize(MSAllocator::FREE_BLOCK_BYTES)
                                + sizeof(RValue::shareLinks);

#include<iostream>
MSAllocator::MSAllocator(uint32_t sizeInBytes) {
    // Pad the size to block size multiple
    uint32_t sizeInBlock = padByteSizeAndConvertToBlockSize(sizeInBytes);
    chunks_[0].chunkStart = new block[sizeInBlock];
    chunks_[0].chunkEnd = chunks_[0].chunkStart + sizeInBlock;
    chunks_[0].nextFastAlloca = chunks_[0].chunkStart;
    chunks_[0].freeListHead = nullptr;
    // Make top env
    makeTopEnvironment();
    // Create singletons
    createNilSingleton();
    createSmallIntSingletions();

    // Create symbol table
    symbolTable_ = new SymbolTable(this);
}

RValue * MSAllocator::allocateInChunk(uint32_t requestedBytes, ChunkData &chunk) {
    // Pad the size to block size multiple
    uint32_t sizeInBlocks = padByteSizeAndConvertToBlockSize(requestedBytes);
    // See if fast alloca is possible
    if (sizeInBlocks + chunk.nextFastAlloca <= chunk.chunkEnd) {
        RValue * result = reinterpret_cast<RValue*>(chunk.nextFastAlloca);
        result->reset(sizeInBlocks, false, 0);
        /*
        result->sizeTotal = sizeInBlocks;
        result->marked = false;*/

        chunk.nextFastAlloca += sizeInBlocks;
        return result;
    }
    // If not scan free list, find suitable spot and allocate its beginning
    RValue * result = allocateFromFreelist(sizeInBlocks, chunk);
    if (result != nullptr) {
        //std::cout << result->sizeTotal << "\n";
        return result;
    }

    // If no room, collect garbage, determine free memory
    mark();
    sweep();

    // Try it again from the free list
    result = allocateFromFreelist(sizeInBlocks, chunk);
    if (result != nullptr) {
        //std::cout << result->sizeTotal << "\n";
        return result;
    }

    throw InterpreterException("Out of heap memory.");
}

RValue * MSAllocator::allocateFromFreelist(uint32_t sizeInBlocks, ChunkData &chunk) {
    RValue *act, *prev;
    prev = nullptr;
    act = chunk.freeListHead;
    while (act != nullptr) {
        if (act->sizeTotal >= sizeInBlocks) { // Free block is good to use
            uint32_t sizeDiff = act->sizeTotal - sizeInBlocks;

            if (sizeDiff < MIN_FREEBLOCK_SIZE_IN_BLOCKS) { // Exact match or not enough room to reduce the free block
                // This freeblock is to be fully claimed, so relink its predecessor
                if (prev == nullptr)
                    chunk.freeListHead = act->freeBlock.next;
                else
                    prev->freeBlock.next = act->freeBlock.next;

                // The size remains.. the block is claimed as a whole
                act->marked = false;
                act->shareLinks = 0;
                return act;
            } else { // The free block can be reduced
                // Reduce the size of the free block and use its end to
                // "allocate" the new value
                act->sizeTotal = sizeDiff;

                // Now move just at the end of the reduced free block
                block * baseForNewRVal = reinterpret_cast<block*>(act);
                baseForNewRVal += act->sizeTotal;
                // Take this adress as the new RValue
                RValue *result = reinterpret_cast<RValue*>(baseForNewRVal);
                result->reset(sizeInBlocks, false, 0);
                /*
                result->sizeTotal = sizeInBlocks;
                result->marked = false;*/
                return result;
            }
        }
        prev = act;
        act = act->freeBlock.next;
    }
    return nullptr;
}

void MSAllocator::setEnvironmentRoot(RValue *env) {
    envRoot_ = env;
}

/*----------------- Allocations ----------------------------------*/

// Vector with one element
RValue * MSAllocator::makeIntegerVector(int value) {
    if (value >= 0 && value < 2)
        return singletons_[value + 1];
    // Room for the one integer is already there
    RValue *result = allocate(INT_VECTOR_BYTES);
    result->type = RValueType::Integer;
    result->intVector.size = 1;
    //result->intVector.data = (int *) (&(result->intVector.data) + 1);
    result->intVector.data[0] = value;
    return result;

    /*
    RValue *result = new RValue();
    result->marked = false;
    result->type = RValueType::Integer;
    result->intVector.size = 1;
    result->intVector.data = new int[1];
    result->intVector.data[0] = value;
    return result;*/
}

// TODO: Assertions on sizes
// Integer vector with given values
RValue * MSAllocator::makeIntegerVector(int * data, int size) {
    // Room for one integer is already there
    RValue *result = allocate(INT_VECTOR_BYTES + sizeof(int) * (size - 1));
    result->type = RValueType::Integer;
    result->intVector.size = size;
    //result->intVector.data = (int *) (&(result->intVector.data) + 1);
    for (int i = 0; i < size; i++)
        result->intVector.data[i] = data[i];
    return result;

    /*
    RValue *result = new RValue();
    result->marked = false;
    result->type = RValueType::Integer;
    result->intVector.size = size;
    result->intVector.data = new int[size];
    for (int i = 0; i < size; i++)
        result->intVector.data[i] = data[i];
    return result;*/
}

// Integer vector with given size
RValue * MSAllocator::makeIntegerVectorS(int size) {
    // Room for one integer is already there
    RValue *result = allocate(INT_VECTOR_BYTES + sizeof(int) * (size - 1));
    result->type = RValueType::Integer;
    result->intVector.size = size;
    //result->intVector.data = (int *) (&(result->intVector.data) + 1);
    return result;

    /*
    RValue *result = new RValue();
    result->marked = false;
    result->type = RValueType::Integer;
    result->intVector.size = size;
    result->intVector.data = new int[size];
    return result;*/
}

// Double vector with one element
RValue * MSAllocator::makeDoubleVector(double value) {
    RValue *result = allocate(DOUBLE_VECTOR_BYTES);
    result->type = RValueType::Double;
    result->dblVector.size = 1;
    //result->dblVector.data = (double *) (&(result->dblVector.data) + 1);
    result->dblVector.data[0] = value;
    return result;

    /*
    RValue *result = new RValue();
    result->marked = false;
    result->type = RValueType::Double;
    result->dblVector.size = 1;
    result->dblVector.data = new double[1];
    result->dblVector.data[0] = value;
    return result;*/
}

// Double vector with given values
RValue * MSAllocator::makeDoubleVector(double * data, int size) {
    RValue *result = allocate(DOUBLE_VECTOR_BYTES + sizeof(double) * (size - 1));
    result->type = RValueType::Double;
    result->dblVector.size = size;
    //result->dblVector.data = (double *) (&(result->dblVector.data) + 1);
    for (int i = 0; i < size; i++)
        result->dblVector.data[i] = data[i];
    return result;

    /*
    RValue *result = new RValue();
    result->marked = false;
    result->type = RValueType::Double;
    result->dblVector.size = size;
    result->dblVector.data = new double[size];
    for (int i = 0; i < size; i++)
        result->dblVector.data[i] = data[i];
    return result;*/
}

// Double vector with given size
RValue * MSAllocator::makeDoubleVectorS(int size) {
    RValue *result = allocate(DOUBLE_VECTOR_BYTES + sizeof(double) * (size - 1));
    result->type = RValueType::Double;
    result->dblVector.size = size;
    //result->dblVector.data = (double *) (&(result->dblVector.data) + 1);
    return result;

    /*
    RValue *result = new RValue();
    result->marked = false;
    result->type = RValueType::Double;
    result->dblVector.size = size;
    result->dblVector.data = new double[size];
    return result;*/
}

// Character vector with the given size. An array of size + 1 chars will be allocated
// and terminating zero will be inserted at the last position
RValue * MSAllocator::makeCharacterVectorS(int size) {
    // We need size + 1 characters, size for actual text, 1 for zero terminator,
    // but one room is already built-in the structure
    RValue *result = allocate(CHAR_VECTOR_BYTES + sizeof(char) * size);
    result->type = RValueType::Character;
    result->charVector.size = size; // Without the terminating zero
    //result->charVector.data = (char *) (&(result->charVector.data) + 1);
    result->charVector.data[size] = '\0';
    return result;

    /*
    RValue *result = new RValue();
    result->marked = false;
    result->type = RValueType::Character;
    result->charVector.size = size; // Does not include the terminating zero
    result->charVector.data = new char[size + 1];
    result->charVector.data[size] = '\0';
    return result;*/
}

// Character vector with the given text
RValue * MSAllocator::makeCharacterVector(const char *str) {
    unsigned size = strlen(str);
    // TODO check
    RValue *result = allocate(CHAR_VECTOR_BYTES + sizeof(char) * (size));
    result->type = RValueType::Character;
    result->charVector.size = size; // Without the terminating zero
    //result->charVector.data = (char *) (&(result->charVector.data) + 1);
    strcpy(result->charVector.data, str);
    return result;

    /*
    RValue *result = new RValue();
    result->marked = false;
    result->type = RValueType::Character;
    result->charVector.size = strlen(str); // Does not include the terminating zero
    result->charVector.data = new char[result->charVector.size];
    strcpy(result->charVector.data, str);
    return result;*/
}

RValue * MSAllocator::makeEnvironmentHolder(Environment *env) {
    RValue *result = allocate(ENVIRONMENT_HOLDER_BYTES);
    result->type = RValueType::EnvironmentHolder;
    result->environment.env = env;
    return result;
    /*
    RValue * result = new RValue();
    result->marked = false;
    result->type = RValueType::EnvironmentHolder;
    result->environment.env = env;
    return result;
    */
}

RValue * MSAllocator::makeLocalEnvironment(RValue * parentHolder) {
    RValue * result = allocate(ENVIRONMENT_HOLDER_BYTES);
    result->type = RValueType::EnvironmentHolder;

    result->environment.parentHolder = parentHolder;
    if (parentHolder == nullptr) {
        result->environment.env = new LocalEnvironment(nullptr);
    } else {
        result->environment.env = new LocalEnvironment(parentHolder->environment.env);
    }
    return result;
}

void MSAllocator::makeTopEnvironment() {
    topEnv_ = makeLocalEnvironment(nullptr);
}

RValue * MSAllocator::makeFunctionDefinition(ast::Function * functionCode, RValue *envHolder,
                                             int argsSize) {
    // The Function definition structure has implicitly defined room for one argument
    // If mode arguments are needed, we need to claim additional space. If the function
    // takes 0 arguments, we are still keeping the one implicitly defined room -- we cannot
    // do anything about it.
    unsigned additionalBytesForArgs = argsSize > 1 ? argsSize - 1 : 0;
    RValue * result = allocate(FUNCTION_DEFINITION_BYTES +
                               sizeof(RValue*) * additionalBytesForArgs);
    result->type = RValueType::FunctionDef;
    // arg count
    result->funcDef.argCount = argsSize;
    // body
    result->funcDef.code = functionCode;
    // home environment
    result->funcDef.envHolder = envHolder;
    return result;
}

RValue * MSAllocator::makeOrGetCharacterVector(const char *str) {
    return symbolTable_->makeOrGet(str);
}

RValue * MSAllocator::getNilSingletion() {
    return singletons_[0];
}

void MSAllocator::createNilSingleton() {
    RValue * nilSingleton = allocate(NIL_BYTES);
    nilSingleton->type = RValueType::Nil;
    singletons_[0] = nilSingleton;
}

void MSAllocator::createSmallIntSingletions() {
    for (int i = 1; i < SINGLETON_COUNT; i++) {
        // Room for the one integer is already there
        RValue *result = allocate(INT_VECTOR_BYTES);
        result->type = RValueType::Integer;
        result->intVector.size = 1;
        //result->intVector.data = (int *) (&(result->intVector.data) + 1);
        result->intVector.data[0] = i - 1; // 0, 1, ...
        singletons_[i] = result; // [1], [2], ...
    }
}

void MSAllocator::testSymbolTable(std::ostream &out) {
    return symbolTable_->test(out);
}

void MSAllocator::sweepValue(RValue *val) {
    switch (val->type) {
    case RValueType::FunctionDef: {
        ASSERTMSG(val->funcDef.code != nullptr,
                  "When sweeping function definition, its body was null!");

        ASSERTMSG(val->funcDef.code->links >= 0, "When sweeping function def., runtime links were already non-positive!");
        ast::Function *code = val->funcDef.code;
        code->links--;
        if (code->links == 0) {
            ast::Function* parent = code->parent;
            while (parent != nullptr) {
                if (parent->links > 0)
                    return; // The code needs to stay alive, as it may be reused
                parent = parent->parent;
            }
            // We will delete
            delete val->funcDef.code;
            val->funcDef.code = nullptr;
        }
        break;
    }
    case RValueType::EnvironmentHolder:
        ASSERTMSG(val->environment.env != nullptr,
                  "When sweeping environment holder, its body was null!");
        delete val->environment.env;
        val->environment.env = nullptr;
    case RValueType::Character: // No outside resources, but it is cached in symbol table
        symbolTable_->remove(val->charVector.data);
    default:
        // Other types have no outside resources
    break;
    }
}

MSAllocator::~MSAllocator() {
    block * act = chunks_[0].chunkStart;
    block * end = chunks_[0].nextFastAlloca;
    while (act < end) {
        RValue * obj = reinterpret_cast<RValue*>(act);
        sweepValue(obj);
        act += obj->sizeTotal;
    }

    if (chunks_[0].chunkStart != nullptr)
        delete [] chunks_[0].chunkStart;

    if (symbolTable_ != nullptr)
        delete symbolTable_;
}

MSAllocator * allocaForKeyVal;
void MSAllocator::markKeyValuePair(RValue *key, RValue *val) {
    allocaForKeyVal->markGenericValue(key);
    allocaForKeyVal->markGenericValue(val);
}

void MSAllocator::markGenericValue(RValue *val) {
    if (val == nullptr)
        return;
    if (val->marked == true)
        return;
    val->marked = true;

    switch (val->type) {
    case RValueType::FunctionDef:
        // Mark formal parameters
        for (unsigned i = 0; i < val->funcDef.argCount; i++) {
            markGenericValue(val->funcDef.formalArgs[i]);
        }
        // Mark captured environment
        markGenericValue(val->funcDef.envHolder);
        break;
    case RValueType::EnvironmentHolder:
        allocaForKeyVal = this;
        val->environment.env->process(markKeyValuePair);
        //markGenericValue(val->environment.env->getParent());
        break;
    default:
        /* Chars, Doubles, Integers and Nils are completely on our heap and they have no
         * other outgoing links to other RValues. Nothing to do in this case. */
        break;
    }
}

void MSAllocator::mark() {
    for (int i = 0; i < SINGLETON_COUNT; i++) {
        markGenericValue(singletons_[i]);
    }
    markGenericValue(envRoot_);

    // Mark temporaries
    if (temporaries != nullptr) {
        std::vector<RValue*> st = *temporaries;
        for (RValue * val : st) {
            markGenericValue(val);
        }
    }
}

void MSAllocator::sweep() {
    //dumpHeap(std::cout);
    block *act = chunks_[0].chunkStart;
    block *end = chunks_[0].nextFastAlloca;
    //std::cout << act << "   " << end << std::endl;
    // List of free block will be reconstructed on the fly
    RValue * newFreeBlockListHead = nullptr;
    while (act < end) {
        // Take the value
        RValue * val = reinterpret_cast<RValue*>(act);
        //std::cout << act << "   " << end << std::endl;
        // Marked allocated value must be left intact, just reset its mark bit and move along
        if (val->marked && !isFree(val)) {
            act += val->sizeTotal;
            val->marked = false;
            continue;
        }

        // Otherwise we will sweep
        // Sweeping phase yields a continuous freeblock
        RValue * rememberedVal = val;
        uint32_t finalFreeBlockSize = 0;

        // Until you reach a marked value or the end of the heap
        while (act < end) {
            val = reinterpret_cast<RValue*>(act);
            if (!isFree(val) && val->marked)
                break;

            // Acummulate the size
            finalFreeBlockSize += val->sizeTotal;
            // Actually sweep the value
            sweepValue(val);

            // Move to next value
            act += val->sizeTotal;
        }


        // Now make this new freeblock the new head of the new freeBlockList
        rememberedVal->type = RValueType::FreeBlock;
        rememberedVal->marked = false;
        rememberedVal->shareLinks = 0;
        rememberedVal->sizeTotal = finalFreeBlockSize;       
        rememberedVal->freeBlock.next = newFreeBlockListHead;
        newFreeBlockListHead = rememberedVal;
    }
    chunks_[0].freeListHead = newFreeBlockListHead;
    //dumpHeap(std::cout);
}











