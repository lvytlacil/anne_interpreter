#ifndef UTIL_H
#define UTIL_H

#include <iostream>

#ifdef DEBUG
#define DEBUGEX(arg) arg
#else
#define DEBUGEX(arg)
#endif

#ifdef DEBUG
#define DOUT(arg) std::cerr << arg << std::endl;
#else
#define DOUT(arg)
#endif


double power(double base, int exp);



#endif // UTIL_H
