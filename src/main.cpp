#include <iostream>
#include <sstream>
#include <fstream>
#include "frontend/parser.h"
#include "interpreter.h"
#include "../test/interpretertest.h"

using std::cin;
using std::istringstream;
using std::string;
using std::cout;
using std::endl;

/**
 * @brief Loads the example of a library.
 * @param al: An allocator to be used to manage the library interpretation.
 */
void loadLib(MSAllocator &al) {
    istringstream is("load(\"lib.an\")");
    try {
        ast::Function * libCode = Parser::parseInput(is);
        interpreter::Interpreter::interpret(libCode->body, al.getTopEnvironment(), &al);
        if (libCode != nullptr)
            delete libCode;
    } catch (ScannerException &se) {
        cout << "(Library load) Scanner error: " << se.what();
    } catch (ParserException &pe) {
        cout << "(Library load) Parser error: " << pe.what();
    } catch (InterpreterException &ie) {
        cout << "(Library load) Interpreter error: " << ie.what();
    }
}

int main(int argc, char *argv[])
{

    InterpreterTest::test(cout);

    // Init code
    MSAllocator al(8192);
    ast::Function * program = nullptr;
    loadLib(al);

    // Welcome
    cout << "Welcome to Anne 1.0.4" << endl;
    cout << "Type --exit to exit." << endl;

    // REPL
    while (true) {
        cout << "> ";
        try {
            string buff;
            getline(cin, buff);
            // Special commands
            if (cin.eof() || buff == "--exit") {
                cout << "Bye." << endl;
                return 0;
            } else if (buff == "--heap") {
                al.dumpHeap(cout);
                continue;
            }

            istringstream is(buff);
            // Parse
            program = Parser::parseInput(is);
            // Interpret
            RValue *result = interpreter::Interpreter::interpret(program->body, al.getTopEnvironment(),
                                                                 &al);
            if (program != nullptr) {
                delete program;
                program = nullptr;
            }

            RValue::dump(result, cout);
           // cout << endl;
        } catch (ScannerException &se) {
            cout << "Scanner error: " << se.what();
        } catch (ParserException &pe) {
            cout << "Parser error: " << pe.what();
        } catch (InterpreterException &ie) {
            cout << "Interpreter error: " << ie.what();
        }
        cout << endl;
    }
    return 0;
}
