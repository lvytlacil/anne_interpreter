#include "environment.h"
#include "runtime.h"
#include <string>

int Environment::counter = 0;

RValue * LocalEnvironment::get(RValue *key) {
    for (int i = 0; i < size_; i++)
        if (key == data_[i].key)
            return data_[i].val;
    if (parent_ != nullptr)
        return parent_->get(key);
    else {
        std::string msg = "Error: Binding not found (";
        msg += (key->type == RValueType::Character ? key->charVector.data : "");
        throw InterpreterException(msg.c_str());
    }
}

void LocalEnvironment::set(RValue *key, RValue *val) {
    // For now, link counting is sound (in the context of improving copy on write)
    // but it is no full. Enabling ADVLINK improves this, but costs time
    // due to another traversing
#ifdef ADVLINK
    for (int i = 0; i < size_; i++)
        if (key == data_[i].key)
            data_[i].val->unlink();
    if (parent_ != nullptr) {
        RValue * original = parent_->get(key);
        original->unlink();
    }
#endif
    for (int i = 0; i < size_; i++)
        if (data_[i].key == key) {
            data_[i].val->unlink();
            data_[i].val = val;
            // Another link to the val
            val->link();
            return;
        }
    if (size_ == capacity_) {
        capacity_ = capacity_ * 1.5 + 1;
        Binding * grow = new Binding[capacity_];
        for (int i = 0; i < capacity_; i++) {
            grow[i].key = nullptr;
            grow[i].val = nullptr;
        }
        memcpy(grow, data_, size_ * sizeof(Binding));
        delete [] data_;
        data_ = grow;
    }
    data_[size_].key = key;
    data_[size_].val = val;
    size_++;
     // Another link to the val
    val->link();
}
