#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H
#include <memory>
#include <cstring>
class RValue;

typedef void (*RValuePairAcceptor)(RValue *, RValue *);

class InterpreterException : public std::exception {
friend class Allocator;
protected:
    std::string msg_;
public:

    InterpreterException(const char * msg): msg_(msg) { msg_ += "\n"; }
    InterpreterException(const char * msg, int row, int col): msg_(msg) {
        msg_ += " (at row ";
        msg_ += std::to_string(row);
        msg_ += ", col ";
        msg_ += std::to_string(col);
        msg_ += ")\n";
    }

    virtual const char * what() const throw() override {
        return msg_.c_str();
    }
};

/**
 * @brief Represents an environment that encapsulates a set of bindings between Character vectors (string constants and identifiers)
 * and runtime values. Note, that these character vectors (keys) are also runtime values: RValues
 */
class Environment {
protected:
    static int counter; // debug
    int id; // debug
    Environment *parent_;
public:
    Environment(Environment * parent)
        :parent_(parent) { id = counter++; }
    virtual ~Environment(){ }

    /**
     * @brief Gets the value associated with this symbol. If the symbol is not bound, exception is thrown.
     * @param symbol
     * @return
     */
    virtual RValue * get(RValue * key) = 0;

    /**
     * @brief Creates a new binding between the given symbol and the runtime value, or updates an
     * existing one, if this symbol is already bound.
     * @param symbol
     * @param val
     */
    virtual void set(RValue * key, RValue * val) = 0;

    /**
     * @brief Clears all bindings
     */
    virtual void clear() = 0;

    /**
     * @brief Processes each key-value pair.
     * @param acc
     */
    virtual void process(RValuePairAcceptor acc) = 0;

    virtual Environment * getParent() {
        return parent_;
    }

    virtual void setParent(Environment * parent) {
        parent_ = parent;
    }
};

/**
 * @brief Environment that uses array for storing and retrieving the bindings
 */
class LocalEnvironment final : public Environment {
private:
    struct Binding {
        RValue *key;
        RValue *val;
    };

    int size_;
    int capacity_;
    Binding * data_;

    RValue * get(RValue * key) override;

    void set(RValue *key, RValue *val) override;


public:
    LocalEnvironment(Environment *parent):Environment(parent), size_(0), capacity_(3) {
        data_ = new Binding[capacity_];
        for (int i = 0; i < capacity_; i++) {
            data_[i].key = nullptr;
            data_[i].val = nullptr;
        }
    }

    ~LocalEnvironment() {
        if (data_ != nullptr)
            delete [] data_;
    }

    void process(RValuePairAcceptor acc) override {
        for (int i = 0; i < size_; i++) {
            acc(data_[i].key, data_[i].val);
        }
    }

    void clear() override {
        size_ = 0;
    }

    Environment * getParent() override {
        return parent_;
    }
};

#endif // ENVIRONMENT_H
