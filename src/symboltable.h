#ifndef SYMBOLTABLE_H
#define SYMBOLTABLE_H

#include <ostream>
#include <vector>

class RValue;
class Allocator;

/**
 * @brief Caches identifiers and string constants (that are CharacterVectors) at runtime, so that we can avoid constructing duplicite
 * identifiers and string constants.
 */
class SymbolTable final {
private:
    const unsigned prime = 31;
    const unsigned initial_size = 10;

    RValue** data_;
    uint32_t size_;
    unsigned storedValuesCount_;
    Allocator * alloca_;

    unsigned hash(const char* ch) {
        unsigned hash = 719; // a prime
        while (*ch != '\0') {
            hash = (hash * prime + *ch) % size_;
            ch++;
        }
        return hash;
    }

    /**
     * @brief Increases the size of the table
     */
    void growTable();

    /**
     * @brief getSlot Gets the free slot index or an index under which the string is already stored
     * @param str string to be hashed
     * @param freeIndex The index
     * @return True, if the given string is already stored, index is then its index. False otherwise and index is
     * then an index of a free position to be used.
     */
    bool getSlot(const char *str, int &freeIndex);


    RValue * makeSymbol(const char *str, int index);



public:
    SymbolTable(Allocator *alloca):
        size_(initial_size), storedValuesCount_(0), alloca_(alloca) {
        data_ = new RValue*[size_];
        for (uint32_t i = 0; i < size_; i++)
            data_[i] = nullptr;
    }

    ~SymbolTable() {
        delete [] data_;
    }


    RValue * makeOrGet(const char *str);
    void remove(const char *str);
    void test(std::ostream &out);
};


#endif // SYMBOLTABLE_H
