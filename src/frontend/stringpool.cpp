#include "stringpool.h"
#include "symbol.h"

namespace pool {

std::size_t Hasher::operator() (Symbol* ps) const {
    const char* ch = ps->value;
    size_t hash = 719; // a prime
    while (*ch != '\0') {
        hash = hash * prime + *ch;
        ch++;
    }
    return hash;
}

bool Comparator::operator() (Symbol *lhs, Symbol *rhs) const {
    return strcmp(lhs->value, rhs->value) == 0;
}

Stringpool::~Stringpool() {
    std::unordered_set<Symbol*, Hasher, Comparator>::const_iterator act = table_.begin();
    std::unordered_set<Symbol*, Hasher, Comparator>::const_iterator finish = table_.end();
    while (act != finish) {
        Symbol* sym = *(act);
        delete [] sym->value;
        delete sym;
        act++;
    }
}

Symbol * Stringpool::addString(const char *str)  {
    Symbol* result = new Symbol(str);
    std::unordered_set<Symbol*, Hasher, Comparator>::const_iterator got = table_.find(result);
    if (got != table_.cend()) {
        delete result;
        return *got;
    } else {
        char* buff = new char[strlen(str) + 1];
        strcpy(buff, str); // Yes, I know.. strcpy..
        result->value = buff;
        table_.insert(result);
        return result;
    }
}

}
