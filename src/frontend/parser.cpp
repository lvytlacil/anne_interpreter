#include "parser.h"
#include "stringpool.h"
#include "../util.h"

#ifdef DEBUG
#define ASSERT(arg) if (!(arg)) throw ParserException("Assertion error during allocator's work");
#define ASSERTMSG(arg, msg) if (!(arg)) throw ParserException(msg);
#else
#define ASSERT(arg)
#define ASSERTMSG(arg, msg)
#endif

// FUNCTION ::= 'function' '(' [IDENTIFIER {',' IDENTIFIER }] ')' SEQ
ast::Function * Parser::parseFunction() {
    ASSERTMSG(func_ != nullptr, "Assertion error: During parseFunction(), parent function was null.");
    // Create and protect a new function
    std::unique_ptr<ast::Function> result (new ast::Function());
    // Hook it to the parent
    if (func_ != nullptr)
        func_->children.push_back(result.get());
    // Store this new function as actual, save the old one
    ast::Function *old = func_;

    // New string pool for this function
    result->pool_ = new pool::Stringpool();
    scanner_->swapStringpool(result->pool_);
    func_ = result.get();

    // Parse the new function
    popAssert(Token::Type::keyFunction);
    popAssert(Token::Type::openPar);
    while (top().type != Token::Type::closePar && top().type != Token::Type::eof) {
        ast::Identifier *param = new ast::Identifier(func_, popAssert(Token::Type::identifier).symVal);
        result->args.push_back(param);
        if (!isTop(Token::Type::comma))
            break;
        popAssert(Token::Type::comma);
    }
    popAssert(Token::Type::closePar);
    result->body = parseSequence();
    // Connect it with its parent
    result->parent = old;

    // Restore old function together with the string pool and return the actual one
    func_ = old;
    scanner_->swapStringpool(old->pool_);
    return result.release();
}
