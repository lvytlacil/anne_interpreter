#ifndef STRINGPOOL_H
#define STRINGPOOL_H

#include <unordered_set>
#include <cstring>

class Symbol;

// TODO: This needs to be checked.. if the hashing works fine
namespace pool {

struct Hasher {
    const size_t prime = 31;
    std::size_t operator()(Symbol* ps) const;
};

struct Comparator {
    bool operator() (Symbol* lhs, Symbol* rhs) const;
};

class Stringpool final {
private:
    std::unordered_set<Symbol*, Hasher, Comparator> table_;
public:
    Stringpool() { }
    ~Stringpool();

    Symbol* addString(const char* str);

    int getSize() {
        return table_.size();
    }
};

}

#endif // STRINGPOOL_H
