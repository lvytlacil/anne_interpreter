#include "scanner.h"
#include "stringpool.h"

std::string Token::typeAsString(Token::Type type) {
    switch (type) {
    case Token::Type::add:
        return "+";
    case Token::Type::assign:
        return "=";
    case Token::Type::closeBracket:
        return "}";
    case Token::Type::closePar:
        return ")";
    case Token::Type::closeSquareBr:
        return "]";
    case Token::Type::comma:
        return ",";
    case Token::Type::dblNum:
        return "double";
    case Token::Type::div:
        return "/";
    case Token::Type::eof:
        return "EOF";
    case Token::Type::eq:
        return "=";
    case Token::Type::gt:
        return ">";
    case Token::Type::identifier:
        return "identifier";
    case Token::Type::intNum:
        return "integer";
    case Token::Type::keyElse:
        return "else(keyword)";
    case Token::Type::keyFunction:
        return "function(keyword)";
    case Token::Type::keyIf:
        return "if(keyword)";
    case Token::Type::keyWhile:
        return "while(keyword)";
    case Token::Type::lt:
        return "<";
    case Token::Type::mul:
        return "*";
    case Token::Type::neq:
        return "!=";
    case Token::Type::openBracket:
        return "{";
    case Token::Type::openPar:
        return "(";
    case Token::Type::openSquareBr:
        return "[";
    case Token::Type::strLiteral:
        return "string";
    case Token::Type::sub:
        return "-";
    default:
        return "unknown";
    }
}

/*-------------------- SCANNER ----------------------------------*/

// 4 is guaranteed by standard (16-bit integer). 9 is for 32bit and
// 18 for 64 bit. Values in between are not considered and are handled safely -
// i.e. for 48bit, 9 digits would be used just like for 32bit
const int Scanner::INT_VALID_DIGIT_COUNT = sizeof(int) >= 4 ? 9 :
                                            (sizeof(int) >= 8 ? 18 : 4);

Scanner::Scanner(std::istream &input) : input_(input) {
    lookahead_ = CHAR_BUFFER_EMPTY;
    row_ = col_ = prevCol_ = 0;
    pool_ = new pool::Stringpool();
}

pool::Stringpool * Scanner::getCurrentAndMakeNewStringpool() {
    pool::Stringpool * result = pool_;
    pool_ = new pool::Stringpool();
    return result;
}

pool::Stringpool * Scanner::getCurrentStringpool() {
    return pool_;
}

pool::Stringpool * Scanner::swapStringpool(pool::Stringpool *sp) {
    pool::Stringpool * result = pool_;
    pool_ = sp;
    return result;
}

void Scanner::skipWhiteSpaces() {
    char next;
    while (getNextChar(next) && isWhiteSpace(next)) { }

    unreadChar(next);
}

void Scanner::skipComments() {
    char next;    
    if (getNextChar(next) && next == ';')
        while (getNextChar(next) && !isNewLine(next)) { }
    else
        unreadChar(next);
}

bool Scanner::peekNextChar(char &ch) {
    if (lookahead_ == CHAR_BUFFER_EMPTY) {

        input_.get(lookahead_);
    }
    ch = lookahead_;
    return input_.good();
}

bool Scanner::getNextChar(char &ch) {
    if (lookahead_ != CHAR_BUFFER_EMPTY) {
        ch = lookahead_;
        lookahead_ = CHAR_BUFFER_EMPTY;
        advancePosition(ch);
        return true;
    }
    bool result = input_.get(ch).good();
    if (result)
        advancePosition(ch);
    return result;
}

void Scanner::advancePosition(char ch) {
    if (ch == '\n') {
        prevCol_ = col_; col_ = 0; row_++;
    } else
        col_++;
}

void Scanner::unreadChar(char ch) {
    if (input_.good()) {
        lookahead_ = ch;
        if (ch == '\n') {
            row_--;
            col_ = prevCol_;
        } else
            col_--;
    }
}

/*
Token Scanner::getNextNumberToken(bool isNegative) {
    char next;
    int num = 0, numLen = 0;
    double dblNum = 0.0;
    int scientificExp = 0;
    bool isDouble = false;
    int decimalPos = 0;

    // Read the number, <digit>*.?<digit>*
    while (getNextChar(next)) {
        if (isDigit(next)) {
            ++numLen;
            // We have exceeded the int size, transfer the reading to double instead
            if (numLen > INT_VALID_DIGIT_COUNT) {
                dblNum = (10.0 * num) + (next - '0');
                while (getNextChar(next)) {
                    if (isDigit(next)) {
                        ++numLen;
                        dblNum = (10.0 * dblNum) + (next - '0');                        
                    } else if (next == '.' && !isDouble) {
                        isDouble = true;
                        decimalPos = numLen;
                    } else {
                        isDouble = true;
                        // Consider the sign
                        if (isNegative)
                            dblNum *= -1;
                        break;
                    }
                }
                break;
            } else
                num = (10 * num) + (next - '0');
        } else if (next == '.' && !isDouble) {
            isDouble = true;
            decimalPos = numLen;
        } else {
            // Consider the sign
            if (isNegative)
                num *= -1;
            break;
        }
    }

    // Allow the scientific notation
    if ( (next == 'e' || next == 'E') && !isEof()) {
        if (!isDouble) {
            decimalPos = numLen;
        }
        isDouble = true;
        bool negativeScientificSign = false;
        // Consider the leading sign
        if (peekNextChar(next)) {
            if (next == '-') {
                negativeScientificSign = true;
                getNextChar(next);
            } else if (next == '+') {
                getNextChar(next);
            }
        }

        while(getNextChar(next) && isDigit(next)) {
            scientificExp = (10 * scientificExp) + (next - '0');
        }

        if (negativeScientificSign)
            scientificExp *= -1;
    }

    unreadChar(next);

    // Create the token
    if (isDouble) {
        double leftDecimalShift = numLen - decimalPos - scientificExp;
        double modifier;
        // The integral part
        if (numLen <= INT_VALID_DIGIT_COUNT)
            dblNum = num;
        if (leftDecimalShift >= 0) {
            modifier = power(10, leftDecimalShift);
            return Token(dblNum / modifier, row_, col_);
        } else {
            modifier = power(10, -leftDecimalShift);
            return Token(dblNum * modifier, row_, col_);
        }

    } else {
        if (numLen > INT_VALID_DIGIT_COUNT)
            return Token(dblNum, row_, col_);
        else
            return Token(num, row_, col_);
    }
}*/

Token Scanner::getNextNumberToken(bool isNegative) {
    char next;
    int numLen = 0;
    bool isDouble = false;
    std::string buffer;

    if (isNegative)
        buffer += "-";

    while (getNextChar(next)) {
        if (isDigit(next)) {
            buffer += next;
            ++numLen;
        } else if (next == '.' && !isDouble) {
            buffer += next;
            isDouble = true;
        } else
            break;
    }

    if ( (next == 'e' || next == 'E') && !isEof()) {
        buffer += next;
        isDouble = true;
        if (peekNextChar(next)) {
            if (next == '-') {
                buffer += next;
                getNextChar(next);
            } else if (next == '+') {
                getNextChar(next);
            }
        }

        while(getNextChar(next) && isDigit(next)) {
            buffer += next;
        }
    }

    unreadChar(next);

    if (isDouble || numLen > INT_VALID_DIGIT_COUNT) {
        return Token(std::stod(buffer), row_, col_);
    } else {
        return Token(std::stoi(buffer), row_, col_);
    }
}

// TODO: Consider scanning errors such as missing trailing "
Token Scanner::getNextStringToken() {
    char next;
    // Keep reading the string content
    std::string result;
    while (getNextChar(next)) {
        if (next == '\"') {
            return Token(Token::Type::strLiteral, pool_->addString(result.c_str()),
                         row_, col_);
        } else if (next == '\\') {
            // Escape characters
            if (getNextChar(next)) {
                switch (next) {
                case 'n':
                    next = '\n';
                    break;
                case 't':
                    next = '\t';
                    break;
                case 'r':
                    next = '\r';
                    break;
                default:
                    break;
                }
            } else {
                //return setError(1);
            }
        }
        result += next;
    }
    //return setError(1);
    throw ScannerException("Non terminated string", row_, col_);
}

Token Scanner::getNextIdentOrKeywordToken(char leadChar) {
    std::string result;
    result += leadChar;
    char next;

    while (getNextChar(next) && (isDigit(next) || isLetter(next)))
        result += next;
    if (!isEof())
        unreadChar(next);

    if (result == "function")
        return Token(Token::Type::keyFunction, row_, col_);
    else if (result == "if")
        return Token(Token::Type::keyIf, row_, col_);
    else if (result == "else")
        return Token(Token::Type::keyElse, row_, col_);
    else if (result == "while")
        return Token(Token::Type::keyWhile, row_, col_);
    else if (result == "array")
        return Token(Token::Type::keyArray, row_, col_);
    else if (result == "len")
        return Token(Token::Type::keyLen, row_, col_);
    else if (result == "load")
        return Token(Token::Type::keyLoad, row_, col_);
    else if (result == "type")
        return Token(Token::Type::keyType, row_, col_);
    else
        return Token(Token::Type::identifier,pool_->addString(result.c_str()),
                     row_, col_);
}
