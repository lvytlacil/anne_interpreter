#ifndef SYMBOL_H
#define SYMBOL_H

namespace pool {
    class Stringpool;
}

struct Symbol final {
    friend class pool::Stringpool;
    const char* value;

    ~Symbol() {

    }

private:
    Symbol(const char* val):
        value(val) { }
};

#endif // SYMBOL_H
