#ifndef PARSER_H
#define PARSER_H

#include <queue>
#include <memory>
#include "scanner.h"
#include "../ast.h"

class ParserException : public std::exception {
private:
    std::string msg_;
public:

    ParserException(const char * msg): msg_(msg) { msg_ += "(Parser)"; }
    ParserException(const char * msg, int row, int col): msg_(msg) {
        msg_ += " (at row ";
        msg_ += std::to_string(row);
        msg_ += ", col ";
        msg_ += std::to_string(col);
        msg_ += ")\n";
    }

    virtual const char * what() const throw() override {
        return msg_.c_str();
    }

};

class Parser
{
private:
    ast::Function * func_; // Points to the actual parsed function
    std::unique_ptr<ast::Function> program; // Holds the program
    Scanner *scanner_;
    Token topToken_; // The lookahead
    bool peeked_; // Whether we have already peeked and the topToken is actual

    Token top() {
        if (!peeked_) {
            topToken_ = scanner_->getNextToken();
            peeked_ = true;
        }
        return topToken_;
    }

    Token pop() {
        if (peeked_) {
            peeked_ = false;
            return topToken_;
        }
        return scanner_->getNextToken();
    }

    Token popAssert(Token::Type expectedType) {
        Token result = pop();
        if (result.type != expectedType)
            throw ParserException("Syntax error, unexpected token", result.row, result.col);
        return result;
    }

    bool isTop(Token::Type givenType) {
        if (!peeked_) {
            topToken_ = scanner_->getNextToken();
            peeked_ = true;
        }
        return topToken_.type == givenType;
    }

    /* Parsing methods for each ast node type. See complete grammar below. */

    // FUNCTION ::= 'function' '(' [IDENTIFIER {',' IDENTIFIER }] ')' SEQ
    ast::Function * parseFunction();

    // CALL ::= '(' [EXPRESSION {',' EXPRESSION }] ')'
    ast::Call * parseCall(ast::Expression * callee) {
        std::unique_ptr<ast::Call> result(new ast::Call(callee));
        popAssert(Token::Type::openPar);        
        while (top().type != Token::Type::closePar && top().type != Token::Type::eof) {
            ast::Expression *arg = parseExpression();
            result->args.push_back(arg);
            if (!isTop(Token::Type::comma))
                break;
            popAssert(Token::Type::comma);
        }
        popAssert(Token::Type::closePar);
        return result.release();
    }

    // ASSIGNMENT ::= '=' EXPRESSION
    ast::Assignment * parseAssignment(std::unique_ptr<ast::Expression> target) {
        ast::Identifier * id = dynamic_cast<ast::Identifier *>(target.get());
        popAssert(Token::Type::assign);        
        if (id == nullptr)
            throw ParserException("Assignment to non-identifier is not permitted");
        target.release();
        std::unique_ptr<ast::Assignment> result (new ast::Assignment(id));
        result->rhs = parseExpression();
        return result.release();
    }

    // INDEX ::= '[' EXPRESSION ']'
    ast::Expression * parseIndexAccess(ast::Expression *target) {
        std::unique_ptr<ast::Index> result(new ast::Index(target));
        popAssert(Token::Type::openSquareBr);        
        result->index = parseExpression();
        popAssert(Token::Type::closeSquareBr);

        // Now an assignment may follow, check it
        if (isTop(Token::Type::assign)) {
            popAssert(Token::Type::assign);
            std::unique_ptr<ast::IndexedAssignment> altResult(new ast::IndexedAssignment(result.release()));
            altResult->rhs = parseExpression();
            return altResult.release();
        }
        return result.release();
    }

    // ARRAY := 'array' '(' [ EXPRESSION {, EXPRESSION } ] ')'
    ast::Expression * parseArrayCall() {
        popAssert(Token::Type::keyArray);
        popAssert(Token::Type::openPar);
        std::unique_ptr<ast::ArrayCall> result(new ast::ArrayCall);
        while (top().type != Token::Type::closePar && top().type != Token::Type::eof) {
            ast::Expression *arg = parseExpression();
            result->args.push_back(arg);
            if (!isTop(Token::Type::comma))
                break;
            popAssert(Token::Type::comma);
        }
        popAssert(Token::Type::closePar);
        return result.release();
    }

    ast::Expression * parseLenCall() {
        popAssert(Token::Type::keyLen);
        popAssert(Token::Type::openPar);
        std::unique_ptr<ast::LenCall> result(new ast::LenCall);
        result->arg = parseExpression();
        popAssert(Token::Type::closePar);
        return result.release();
    }

    ast::Expression * parseLoadCall() {
        popAssert(Token::Type::keyLoad);
        popAssert(Token::Type::openPar);
        std::unique_ptr<ast::LoadCall> result(new ast::LoadCall);
        result->arg = parseExpression();
        popAssert(Token::Type::closePar);
        return result.release();
    }

    ast::Expression * parseTypeCall() {
        popAssert(Token::Type::keyType);
        popAssert(Token::Type::openPar);
        std::unique_ptr<ast::TypeCall> result(new ast::TypeCall);
        result->arg = parseExpression();
        popAssert(Token::Type::closePar);
        return result.release();
    }

    // ELEMENT ::= IDENTIFIER | INTEGER | DOUBLE | STRING | FUNCTION | BUILT_IN_CALL
    //              | '(' EXPRESSION ')' | SEQ
    // BUILT_IN_CALL ::= ARRAY
    ast::Expression * parseElement() {
        switch (top().type) {
        case Token::Type::identifier:
            return new ast::Identifier(func_, pop().symVal);
            break;
        case Token::Type::intNum:
            return new ast::IntNumber(pop().intVal);
            break;
        case Token::Type::dblNum:
            return new ast::RealNumber(pop().dblVal);
            break;
        case Token::Type::strLiteral:
            return new ast::StringLit(func_, pop().symVal);
            break;
        case Token::Type::keyFunction:
            return parseFunction();
        case Token::Type::openPar: {
            pop();
            std::unique_ptr<ast::Expression> result(parseExpression());
            popAssert(Token::Type::closePar);
            return result.release();
        }
        case Token::Type::openBracket:
            return parseSequence();
        case Token::Type::keyArray:
            return parseArrayCall();
        case Token::Type::keyLen:
            return parseLenCall();
        case Token::Type::keyLoad:
            return parseLoadCall();
        case Token::Type::keyType:
            return parseTypeCall();

        default:
            throw ParserException("Invalid expression");
        }
    }

    // F ::= ELEMENT { INDEX | CALL | ASSIGNMENT }
    ast::Expression * parseF() {
        std::unique_ptr<ast::Expression> result(parseElement());
        while (true) {
            switch (top().type) {
            case Token::Type::openSquareBr:
                result.reset(parseIndexAccess(result.release()));
                break;
            case Token::Type::openPar:
                result.reset(parseCall(result.release()));
                break;
            case Token::Type::assign:
                result.reset(parseAssignment(std::move(result)));
                break;
            default:
                return result.release();
            }
        }
    }

    // T ::= F { (* | /) F }
    ast::Expression * parseTerm() {
        std::unique_ptr<ast::Expression> result(parseF());

        while (true) {
            switch (top().type) {
            case Token::Type::mul:
            case Token::Type::div: {
                ast::BinaryExp::OpCode opCode;
                switch(pop().type) {
                case Token::Type::mul:
                    opCode = ast::BinaryExp::OpCode::mul;
                    break;
                case Token::Type::div:
                    opCode = ast::BinaryExp::OpCode::div;
                    break;
                default:
                    // not reached
                    break;
                }
                ast::Expression * right = parseF();
                result.reset(new ast::BinaryExp(result.release(), right, opCode));
                break;
            }
            default:
                return result.release();
            }
        }
    }

    // E ::= T { (+ | -) T}
    ast::Expression * parseE() {
        std::unique_ptr<ast::Expression> result(parseTerm());

        while (true) {
            switch (top().type) {
            case Token::Type::add:
            case Token::Type::sub: {
                ast::BinaryExp::OpCode opCode;
                switch (pop().type) {
                case Token::Type::add:
                    opCode = ast::BinaryExp::OpCode::add;
                    break;
                case Token::Type::sub:
                    opCode = ast::BinaryExp::OpCode::sub;
                    break;
                default:
                    // not reached
                    break;
                }
                ast::Expression * right = parseTerm();
                result.reset(new ast::BinaryExp(result.release(),right, opCode));
                break;
            }
            default:
                return result.release();
            }
        }
    }

    // EXPRESSION ::= E [ (== | != | < | <= | > | >=) E]
    ast::Expression * parseExpression() {
        std::unique_ptr<ast::Expression> left(parseE());

        switch (top().type) {
        case Token::Type::eq:
        case Token::Type::neq:
        case Token::Type::lt:
        case Token::Type::gt: {
            ast::BinaryExp::OpCode opCode;
            switch (pop().type) {
            case Token::Type::eq:
                opCode = ast::BinaryExp::OpCode::eq;
                break;
            case Token::Type::neq:
                opCode = ast::BinaryExp::OpCode::neq;
                break;
            case Token::Type::lt:
                opCode = ast::BinaryExp::OpCode::lt;
                break;
            case Token::Type::gt:
                opCode = ast::BinaryExp::OpCode::gt;
                break;
            default:
                // unreachable
                break;
            }
            ast::Expression * right = parseE();
            return new ast::BinaryExp(left.release(), right, opCode);
        }
        default:
            return left.release();
        }
    }

    // WHILE ::= 'while' '(' EXPRESSION ')' SEQ
    ast::LoopExp * parseLoop() {
        popAssert(Token::Type::keyWhile);
        popAssert(Token::Type::openPar);
        std::unique_ptr<ast::LoopExp> result(new ast::LoopExp(parseExpression()));
        popAssert(Token::Type::closePar);
        result->body = parseSequence();
        return result.release();
    }

    // IF ::= 'if' '(' EXPRESSION ')' SEQUENCE ['else' SEQUENCE]
    ast::ConditionalExp * parseConditional() {
        popAssert(Token::Type::keyIf);
        popAssert(Token::Type::openPar);
        std::unique_ptr<ast::ConditionalExp> result(new ast::ConditionalExp(parseExpression()));
        popAssert(Token::Type::closePar);

        result->trueCase = parseSequence();
        if (isTop(Token::Type::keyElse)) {
            popAssert(Token::Type::keyElse);
            result->falseCase = parseSequence();
        }
        else
            result->falseCase = new ast::IntNumber(0);
        return result.release();
    }

    // STATEMENT ::= IF | WHILE | EXPRESSION
    ast::Expression * parseStatement() {
        switch (top().type) {
        case Token::Type::keyIf:
            return parseConditional();
        case Token::Type::keyWhile:
            return parseLoop();
        default:
            return parseExpression();
        }
    }

    // SEQUENCE ::= '{' {STATEMENT} '}'
    ast::Sequence * parseSequence() {
        std::unique_ptr<ast::Sequence> result(new ast::Sequence());
        popAssert(Token::Type::openBracket);
        // Parse the body of the sequence
        while (top().type != Token::Type::closeBracket && top().type != Token::Type::eof) {
            result->body.push_back(parseStatement());
        }
        popAssert(Token::Type::closeBracket);
        return result.release();
    }

    // PROGRAM ::= {STATEMENT}
    ast::Function * parseProgram() {
        // Program is a function only formally, it is really a sequence of statements
        // But we need this to incorporate string pools.
        program.reset(new ast::Function());
        program->pool_ = scanner_->getCurrentStringpool(); // the initial scanner string pool
        // Store the function
        func_ = program.get();

        // Now parse body
        std::unique_ptr<ast::Sequence> programStatements(new ast::Sequence());

        while (top().type != Token::Type::eof) {
            ast::Expression * st = parseStatement();
            programStatements->body.push_back(st);
        }        
        program->body = programStatements.release();
        return program.release();
    }



public:
    Parser(Scanner * scanner):scanner_(scanner), topToken_(Token::Type::eof, 0, 0),
        peeked_(false) { }

    static ast::Function * parseInput(std::istream &input) {
        Scanner scanner_(input);
        Parser parser(&scanner_);

        return parser.parseProgram();
    }

    /*
     * PROGRAM ::= { STATEMENT }
     * STATEMENT ::= IF | WHILE | EXPRESSION
     * IF ::= 'if' '(' EXPRESSION ')' SEQUENCE ['else' SEQUENCE]
     * WHILE ::= 'while' '(' EXPRESSION ')' SEQ
     * SEQUENCE ::= '{' { STATEMENT } '}'
     * EXPRESSION ::= E [ (== | != | < | >) E]
     * E ::= T { (+ | -) T }
     * T ::= F { (* | /) F }
     * F ::= ELEMENT { INDEX | CALL | ASSIGNMENT }
     * INDEX ::= '[' EXPRESSION ']'
     * CALL ::= '(' [EXPRESSION {',' EXPRESSION }] ')'
     * ASSIGNMENT ::= '=' EXPRESSION
     * ELEMENT ::= IDENTIFIER | INTEGER | DOUBLE | STRING | FUNCTION | BUILT_IN_CALL
     *              | '(' EXPRESSION ')' | SEQ
     * BUILT_IN_CALL ::= ARRAY | LEN | LOAD
     * ARRAY := 'array' '(' [ EXPRESSION {, EXPRESSION } ] ')'
     * LEN := 'len' '(' EXPRESSION ')'
     * LOAD := 'load' '(' EXPRESSION ')'
     * TYPE := 'type' '(' EXPRESSION ')'
     * FUNCTION ::= 'function' '(' [IDENTIFIER {',' IDENTIFIER }] ')' SEQ
     */
};

#endif // PARSER_H
