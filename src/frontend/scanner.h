#ifndef SCANNER_H
#define SCANNER_H

#include <istream>
#include <exception>
#include "../util.h"
#include "symbol.h"

namespace pool {
    class Stringpool;
}

/**
 * @brief Represents a scanned element
 */
class Token {
public:
    enum class Type {
        eof,
        dblNum,
        intNum,
        strLiteral,
        identifier,
        keyIf,
        keyElse,
        keyWhile,
        keyFunction,
        keyArray,
        keyLen,
        keyLoad,
        keyType,
        add,
        sub,
        mul,
        div,
        eq,
        neq,
        lt,
        gt,
        assign,
        comma,
        openPar,
        closePar,
        openBracket,
        closeBracket,
        openSquareBr,
        closeSquareBr
    };

    union {
        int intVal;
        double dblVal;
        Symbol* symVal;
    };

    Type type;
    int row;
    int col;


    Token(Type t, int rowNum, int colNum):
        type(t), row(rowNum), col(colNum) { }

    Token (double val, int rowNum, int colNum):
        dblVal(val),
        type(Type::dblNum),
        row(rowNum), col(colNum){ }

    Token (int val, int rowNum, int colNum):
        intVal(val),
        type(Type::intNum),
        row(rowNum), col(colNum) { }

    Token (Type t, Symbol* sym, int rowNum, int colNum):
        symVal(sym), type(t),
        row(rowNum), col(colNum) { }

    static std::string typeAsString(Token::Type type);
};

class ScannerException : public std::exception {
protected:
    std::string msg_;
public:

    ScannerException(const char * msg): msg_(msg) { }
    ScannerException(const char * msg, int row, int col): msg_(msg) {
        msg_ += " (at row ";
        msg_ += std::to_string(row);
        msg_ += ", col ";
        msg_ += std::to_string(col);
        msg_ += ")";
    }

    virtual const char * what() const throw() override {
        return msg_.c_str();
    }

};

/**
 * @brief The Scanner class, that allows to scan the given input stream
 * and extract tokens from it.
 */
class Scanner
{
private:
    static const char CHAR_BUFFER_EMPTY = '\0';
    // no. digits that fit into the int type
    static const int INT_VALID_DIGIT_COUNT;

    std::istream &input_;
    char lookahead_;
    int row_;
    int col_;
    int prevCol_; // Stores the old column number in case a new line is unread
    pool::Stringpool * pool_; // The string pool the scanner uses to pool scanned strings

    bool peekNextChar(char &ch);
    bool getNextChar(char &ch);
    /* Calling unreadChar more than once without calling getNextChar in between might
     * corrupt row_ and col_ numbers */
    void unreadChar(char ch);
    void skipWhiteSpaces();
    void skipComments();
    inline void advancePosition(char ch);

    Token getNextNumberToken(bool isNegative);
    Token getNextStringToken();
    Token getNextIdentOrKeywordToken(char leadChar);

    bool isEof() {
        return input_.eof() && lookahead_ == CHAR_BUFFER_EMPTY;
    }

    bool isDigit(char ch) const {
        return ch >= '0' && ch <= '9';
    }

    bool isDigitOrDot(char ch) const {
        return (ch >= '0' && ch <= '9') || ch == '.';
    }

    bool isWhiteSpace(char ch) const {
        return (ch == ' ' || ch == '\t' || ch == '\n' || ch == '\r');
    }

    bool isLetter(char ch) const {
        return ( (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'));
    }

    bool isNewLine(char ch) const {
        return ch == '\n';
    }

public:
    Scanner(std::istream &input);

    /**
     * @brief Returns the underlying Stringpool giving up its ownership and creates and starts using a new one.
     * @return
     */
    pool::Stringpool * getCurrentAndMakeNewStringpool();
    /**
     * @brief Returns the underlying Stringpool, but continues to use it internally.
     * @return
     */
    pool::Stringpool * getCurrentStringpool();

    /**
     * @brief Returns the underlying stringpool and swaps it internally with the passed one
     * @param sp
     * @return
     */
    pool::Stringpool * swapStringpool(pool::Stringpool * sp);

    /**
     * @brief Scans the next token from the underlying input.
     * @return The next extracted token.
     * @throw ScannerException might be thrown on an input that doesn't follow lexical rules of the language.
     */
    Token getNextToken() {
        skipWhiteSpaces();
        skipComments();
        char next;
        getNextChar(next);
        if (isEof())
            return Token(Token::Type::eof, row_, col_);

        switch (next) {
        case '+':
            return Token(Token::Type::add, row_, col_);
        case '-':            
            return Token(Token::Type::sub, row_, col_);
        case '~':
            return getNextNumberToken(true);
        case '*':
            return Token(Token::Type::mul, row_, col_);
        case '/':
            return Token(Token::Type::div, row_, col_);
        case '=':
            peekNextChar(next);
            if (next == '=') {
                getNextChar(next);
                return Token(Token::Type::eq, row_, col_);
            }
            return Token(Token::Type::assign, row_, col_);
        case '!':
            getNextChar(next);
            if (next != '=')
                throw ScannerException("Error: Expected !=, but only ! found.", row_, col_);
            return Token(Token::Type::neq, row_, col_);
        case '<':
            return Token(Token::Type::lt, row_, col_);
        case '>':
            return Token(Token::Type::gt, row_, col_);
        case ',':
            return Token(Token::Type::comma, row_, col_);
        case '(':
            return Token(Token::Type::openPar, row_, col_);
        case ')':
            return Token(Token::Type::closePar, row_, col_);
        case '[':
            return Token(Token::Type::openSquareBr, row_, col_);
        case ']':
            return Token(Token::Type::closeSquareBr, row_, col_);
        case '{':
            return Token(Token::Type::openBracket, row_, col_);
        case '}':
            return Token(Token::Type::closeBracket, row_, col_);
        case '\"':
            return getNextStringToken();
        default:
            if (isDigitOrDot(next)) {
                unreadChar(next);
                return getNextNumberToken(false);
            }
            else if(isLetter(next)) {
                return getNextIdentOrKeywordToken(next);
            } else {
                throw ScannerException("Illegal character", row_, col_);
            }
        }
    }
};

#endif // SCANNER_H
