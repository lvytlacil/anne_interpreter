#include "symboltable.h"

#include <cstring>
#include <ostream>
#include "runtime.h"
#include "allocator.h"

#ifdef DEBUG
#define ASSERT(arg) if (!(arg)) throw "Assertion error during interpretation";
#define ASSERTMSG(arg, msg) if (!(arg)) throw msg;
#else
#define ASSERT(arg)
#define ASSERTMSG(arg, msg)
#endif


void SymbolTable::growTable() {
    uint32_t oldSize = size_;
    size_ *= 2; // TODO, assure size_ is prime for good hashing
    RValue** tmp = data_;
    data_ = new RValue*[size_];

    for (uint32_t i = 0; i < size_; i++)
        data_[i] = nullptr;

    for (unsigned i = 0; i < oldSize; i++) { // Rehash old values
        RValue *oldValue = tmp[i];
        if (oldValue != nullptr) {
            int newIndex = 0;
            bool r = getSlot(oldValue->charVector.data, newIndex);
            ASSERTMSG(r == false, "Assertion: For some fucking reason, when growing symbol table, duplicities were created.");
            data_[newIndex] = oldValue;
        }
    }
    delete [] tmp;
}

bool SymbolTable::getSlot(const char *str, int &freeIndex) {
    uint32_t h = hash(str);
    uint32_t index = h % size_;
    uint32_t initialIndex = index; // Store the inital value, so we can later break the loop

    while(true) {
        RValue * result = data_[index];

        if (result == nullptr) { // Free slot found
            freeIndex = index;
            return false;
        }

        if (strcmp(str, result->charVector.data) == 0) { // The symbol is already there, cool
            freeIndex = index;
            return true;
        }

        index = (index + 1) % size_; // Try again
        if (index == initialIndex) { // starting over, but that is an error
            ASSERTMSG(0, "Symbol table getSlot failed");
        }
    }
}

RValue * SymbolTable::makeSymbol(const char *str, int index) {
    RValue * result = alloca_->makeCharacterVector(str);
    data_[index] = result;

    // Grow the table if necessary
    storedValuesCount_++;
    if (storedValuesCount_ > (size_ * 3) / 4)
        growTable();

    return result;
}

RValue * SymbolTable::makeOrGet(const char *str) {
    int freeIndex = 0;
    bool isThere = getSlot(str, freeIndex);

    if (isThere) {
        RValue * result = data_[freeIndex];
        return result;
    } else {
        return makeSymbol(str, freeIndex);
    }
}

void SymbolTable::remove(const char *str) {
    int freeIndex = 0;
    bool isThere = getSlot(str, freeIndex);

    if (isThere) {
        data_[freeIndex] = nullptr;
        storedValuesCount_--;
        // TODO: You may shrink it at 1 / 4 of capacity
    }
}



/*----------- TEST -----------------*/

void SymbolTable::test(std::ostream &out) {
    out << "SYMBOL TABLE TEST\n";
    out << "Few hashed values:\n";
    out << "fire: " << hash("fire") << "\n";
    out << "fearie: " << 	hash("fearie") << "\n";
    out << "frost: " << hash("frost") << "\n";
    out << "icy: " << hash("icy") << "\n";

    out << "Storing: fire, lead\n";
    makeOrGet("frostfire");
    makeOrGet("lead");
    out << "no. stored items: " << storedValuesCount_ << "\n";
    for (unsigned i = 0; i < size_; i++) {
        RValue * r = data_[i];
        if (r == nullptr)
            out << " - ";
        else
            out << r->charVector.data << " ";
        out << "\n";
    }
    out << "Push more items...\n";
    makeOrGet("grip");
    makeOrGet("bestow");
    makeOrGet("veronika");
    makeOrGet("tip");
    makeOrGet("react");
    makeOrGet("slice");
    makeOrGet("column");

    for (unsigned i = 0; i < size_; i++) {
        RValue * r = data_[i];
        if (r == nullptr)
            out << " - ";
        else
            out << r->charVector.data << " ";
        out << "\n";
    }
    out << "I will now push some items that have already been added\n";
    out << "no. stored items before: " << storedValuesCount_ << "\n";
    makeOrGet("react");
    makeOrGet("veronika");
    out << "no. stored items now: " << storedValuesCount_ << "\n";

    for (unsigned i = 0; i < size_; i++) {
        RValue * r = data_[i];
        if (r == nullptr)
            out << " - ";
        else
            out << r->charVector.data << " ";
        out << "\n";
    }
}
