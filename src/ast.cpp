#include "ast.h"
#include "astprocessor.h"
#include "frontend/stringpool.h"
#include "frontend/symbol.h"

const char * ast::Identifier::getName() {
    return symbol->value;
}

const char * ast::StringLit::getName() {
    return symbol->value;
}

ast::Function::~Function() {
    for (ast::Function *child : children)
        child->parent = nullptr;

    for (Identifier *arg : args) {
        DELPTR(arg);
    }
    DELPTR(body);
    if (pool_ != nullptr)
        delete pool_;
}

void ast::IntNumber::processBy(AstProcessor *proc) {
    proc->process(this);
}

void ast::Assignment::processBy(AstProcessor *proc) {
    proc->process(this);
}

void ast::BinaryExp::processBy(AstProcessor *proc) {
    proc->process(this);
}

void ast::Call::processBy(AstProcessor *proc) {
    proc->process(this);
}

void ast::ArrayCall::processBy(AstProcessor *proc) {
    proc->process(this);
}

void ast::LenCall::processBy(AstProcessor *proc) {
    proc->process(this);
}

void ast::LoadCall::processBy(AstProcessor *proc) {
    proc->process(this);
}

void ast::TypeCall::processBy(AstProcessor *proc) {
    proc->process(this);
}

void ast::ConditionalExp::processBy(AstProcessor *proc) {
    proc->process(this);
}

void ast::Expression::processBy(AstProcessor *proc) {
    proc->process(this);
}

void ast::Function::processBy(AstProcessor *proc) {
    proc->process(this);
}

void ast::Identifier::processBy(AstProcessor *proc) {
    proc->process(this);
}

void ast::Index::processBy(AstProcessor *proc) {
    proc->process(this);
}

void ast::IndexedAssignment::processBy(AstProcessor *proc) {
    proc->process(this);
}

void ast::LoopExp::processBy(AstProcessor *proc) {
    proc->process(this);
}

void ast::RealNumber::processBy(AstProcessor *proc) {
    proc->process(this);
}

void ast::Sequence::processBy(AstProcessor *proc) {
    proc->process(this);
}

void ast::StringLit::processBy(AstProcessor *proc) {
    proc->process(this);
}
