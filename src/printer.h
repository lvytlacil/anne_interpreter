#ifndef PRINTER_H
#define PRINTER_H

#include "astprocessor.h"

/**
 * @brief This class represents a AST Printer, that can walk through the AST and print its nodes.
 * This is useful for debugging.
 */
class Printer final : public AstProcessor
{
private:
    std::ostream &out_;
    int padding_;

    void makeNewLine() {
        out_ << "\n";
        for (int i = 0; i < padding_; i++)
            out_ << " ";
    }

    void incPadding() {
        padding_ += 2;
    }

    void decPadding() {
        padding_ -= 2;
    }

public:
    Printer(std::ostream &out):
        out_(out), padding_(0) { }

    void process(ast::Expression *node) override {
        out_ << "genericExpression";
    }

    void process(ast::Assignment *node) override {
        node->dest->processBy(this);
        out_ << "= ";
        node->rhs->processBy(this);
    }

    void process(ast::BinaryExp *node) override {
        node->lhs->processBy(this);

        switch (node->opCode) {
        case ast::BinaryExp::OpCode::add:
            out_ << "+";
            break;
        case ast::BinaryExp::OpCode::div:
            out_ << "/";
            break;
        case ast::BinaryExp::OpCode::eq:
            out_ << "==";
            break;
        case ast::BinaryExp::OpCode::gt:
            out_ << ">";
            break;
        case ast::BinaryExp::OpCode::lt:
            out_ << "<";
            break;
        case ast::BinaryExp::OpCode::mul:
            out_ << "*";
            break;
        case ast::BinaryExp::OpCode::neq:
            out_ << "!=";
            break;
        case ast::BinaryExp::OpCode::sub:
            out_ << "-";
            break;
        default:
            // Should not be reached
            break;
        }
        out_ << " ";
        node->rhs->processBy(this);
    }

    void process(ast::Call *node) override {
        node->callee->processBy(this);
        out_ << "(";
        for (ast::Expression *arg : node->args) {
            arg->processBy(this);
            out_ << ", ";
        }
        out_ << ")";
        makeNewLine();
    }



    void process(ast::ArrayCall *node) override {
        out_ << "(";
        for (ast::Expression *arg : node->args) {
            arg->processBy(this);
            out_ << ", ";
        }
        out_ << ")";
        makeNewLine();
    }

    void process(ast::LoadCall *node) override {
        out_ << "load(";
        node->arg->processBy(this);
        out_ << ")";
        makeNewLine();
    }

    void process(ast::LenCall *node) override {
        out_ << "len(";
        node->arg->processBy(this);
        out_ << ")";
        makeNewLine();
    }

    void process(ast::TypeCall *node) override {
        out_ << "type(";
        node->arg->processBy(this);
        out_ << ")";
        makeNewLine();
    }

    void process(ast::ConditionalExp *node) override {
        out_ << "if (";
        node->cond->processBy(this);
        out_ << ") ";
        node->trueCase->processBy(this);
        node->falseCase->processBy(this);
    }

    void process(ast::Function *node) override {
        out_ << "function (";
        for (ast::Identifier *id : node->args) {
            id->processBy(this);
            out_ << ", ";
        }
        out_ << ")";


        makeNewLine();
        node->body->processBy(this);
        makeNewLine();
    }

    void process(ast::Identifier *node) override {
        out_ << node->getName();
        out_ << " ";
    }

    void process(ast::Index *node) override {
        node->identifier->processBy(this);
        out_ << "[";
        node->index->processBy(this);
        out_ << "] ";
    }

    void process(ast::IndexedAssignment *node) override {
        node->dest->processBy(this);
        out_ << " = ";
        node->rhs->processBy(this);
    }

    void process(ast::IntNumber *node) override {
        out_ << node->value << " ";
    }

    void process(ast::LoopExp *node) override {
        out_ << "while (";
        node->cond->processBy(this);
        out_ << ")";
        makeNewLine();
        node->body->processBy(this);
    }

    void process(ast::RealNumber *node) override {
        out_ << node->value << " ";
    }

    void process(ast::Sequence *node) override {
        out_ << "{";
        for (ast::Expression *exp : node->body) {
            exp->processBy(this);
        }
        out_ << "}";
        makeNewLine();
    }

    void process(ast::StringLit *node) override {
        out_ << "\"";
        out_ << node->getName();
        out_ << "\"";
    }
};

#endif // PRINTER_H
