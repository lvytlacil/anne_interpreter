#ifndef ASTPROCESSOR_H
#define ASTPROCESSOR_H

#include "ast.h"

/**
 * @brief Represents an interface, that every class, that is able to systematically walk
 * through a AST and do some operations should implement. Default implementation does literally nothing --
 * it delegates processing of every node to the processing of ast::Expression and that just does nothing.
 */
class AstProcessor {
public:
    virtual void process(ast::Expression * node) { }
    virtual void process(ast::Assignment * node) {
        process(static_cast<ast::Expression*>(node));
    }
    virtual void process(ast::BinaryExp * node) {
        process(static_cast<ast::Expression*>(node));
    }
    virtual void process(ast::Call * node) {
        process(static_cast<ast::Expression*>(node));
    }    

    virtual void process(ast::ArrayCall * node) {
        process(static_cast<ast::Expression*>(node));
    }
    virtual void process(ast::LenCall * node) {
        process(static_cast<ast::Expression*>(node));
    }
    virtual void process(ast::LoadCall * node) {
        process(static_cast<ast::Expression*>(node));
    }
    virtual void process(ast::TypeCall * node) {
        process(static_cast<ast::Expression*>(node));
    }
    virtual void process(ast::ConditionalExp * node) {
        process(static_cast<ast::Expression*>(node));
    }
    virtual void process(ast::Function * node) {
        process(static_cast<ast::Expression*>(node));
    }
    virtual void process(ast::Identifier * node) {
        process(static_cast<ast::Expression*>(node));
    }
    virtual void process(ast::Index * node) {
        process(static_cast<ast::Expression*>(node));
    }
    virtual void process(ast::IndexedAssignment * node) {
        process(static_cast<ast::Expression*>(node));
    }
    virtual void process(ast::IntNumber * node) {
        process(static_cast<ast::Expression*>(node));
    }
    virtual void process(ast::LoopExp * node) {
        process(static_cast<ast::Expression*>(node));
    }
    virtual void process(ast::RealNumber * node) {
        process(static_cast<ast::Expression*>(node));
    }
    virtual void process(ast::Sequence * node) {
        process(static_cast<ast::Expression*>(node));
    }
    virtual void process(ast::StringLit * node) {
        process(static_cast<ast::Expression*>(node));
    }

};

#endif // ASTPROCESSOR_H
