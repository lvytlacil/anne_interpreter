#ifndef RUNTIME_H
#define RUNTIME_H

#include <ostream>
#include "ast.h"
#include <iostream>

class Environment;
struct RValue;

struct DoubleVector final {
    uint32_t size;
    double data[1];
};

struct IntegerVector final {
    uint32_t size;
    int data[1];
};

struct CharacterVector final {
    uint32_t size; // Not including zero-terminator
    char data[1];
};

struct EnvironmentHolder {
    Environment *env;
    RValue * parentHolder;
};

struct FunctionDefinition {
    uint16_t argCount;
    RValue* envHolder;
    ast::Function* code;
    RValue* formalArgs[1];
};

struct FF {
    RValue *next;
};



//--------------------------

enum class RValueType : uint8_t {
    Double,
    Integer,
    Character,
    FunctionDef,
    EnvironmentHolder,
    FreeBlock,
    Nil
};

struct RValue
{
    uint32_t sizeTotal; // Most significant bit is reserved
    bool marked;
    RValueType type;
    uint8_t shareLinks; // To support efficient copy on write in index assignments

    /* THIS UNION MUST REMAIN LAST DATA FIELD !!!!!! Because all those arrays of size 1
    can be actually interpreted as a arrays of multiple elements. */
    union {
        DoubleVector dblVector;
        IntegerVector intVector;
        CharacterVector charVector;
        FunctionDefinition funcDef;
        EnvironmentHolder environment;
        FF freeBlock;
    };


    // Convinience method for setting values commonly needed to be set during allocation
    void reset(uint32_t sizeTotal, bool marked, uint8_t shareLinks) {
        this->sizeTotal = sizeTotal;
        this->marked = marked;
        this->shareLinks = shareLinks;
    }

    // If linked more than 255 times, we would lose information and cannot recover, because we only store it
    // in 8 bits. So, in that case, it will remain "linked" forever, preventing it from mutation.

    void link() {
        if (shareLinks < 255)
            shareLinks++;
    }

    void unlink() {
        if (shareLinks < 255)
            shareLinks--;
    }

    static const char * typeToString(RValueType type) {
        switch (type) {
        case RValueType::Integer:
            return "integer";
        case RValueType::EnvironmentHolder:
            return "environment";
        case RValueType::Character:
            return "character";
        case RValueType::FunctionDef:
            return "function";
        case RValueType::Nil:
            return "nil";
        case RValueType::FreeBlock:
            return "freeblock";
        case RValueType::Double:
            return "double";
        default:
            return "???";
        }
    }

    static void dump(RValue *val, std::ostream &out);
};

//--------------------------





/* Symbol table */



#endif // RUNTIME_H
