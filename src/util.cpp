#include "util.h"

static void error(const char *txt) {
    std::cerr << txt << std::endl;
}

#ifdef DEBUG
#define ASSERT(cond, msg) if (!(cond)) error(msg);
#else
#define ASSERT(cond, msg)
#endif

double power(double base, int exp) {
    ASSERT((exp >= 0), "util.cpp::power - argument exp is negative");
    double result = 1.0;
    while (exp > 0) {
        if (exp % 2 == 1)
            result *= base;
        exp >>= 1;
        base *= base;
    }
    return result;
}
