#include "runtime.h"
#include "printer.h"

void RValue::dump(RValue *val, std::ostream &out) {
    switch (val->type) {
    case RValueType::Integer:
        out << "Int (";
        for (uint32_t i = 0; i < val->intVector.size - 1; i++) {
            out << val->intVector.data[i]; out << ", ";
        }
        out << val->intVector.data[val->intVector.size - 1];
        out << ")";
        break;
    case RValueType::Double:
        out << "Double (";
        for (uint32_t i = 0; i < val->dblVector.size - 1; i++) {
            out << val->dblVector.data[i]; out << ", ";
        }
        out << val->dblVector.data[val->dblVector.size - 1];
        out << ")";
        break;
    case RValueType::EnvironmentHolder:
        out << "Environment";
        break;
    case RValueType::Character:
        out << "\"";
        out << val->charVector.data;
        out << "\"";
        out << " (Character)";
        break;
    case RValueType::FunctionDef: {
        out << "Function";
        break;
    }
    case RValueType::Nil:
        out << "NIL";
        break;
    case RValueType::FreeBlock:
        out << "Free heap block";
        break;
    }
}
