#include <memory>
#include <cmath>
#include <fstream>
#include "frontend/parser.h"
#include "interpreter.h"

namespace interpreter {

#ifdef DEBUG
#define ASSERT(arg) if (!(arg)) throw InterpreterException("Assertion error during interpretation");
#define ASSERTMSG(arg, msg) if (!(arg)) throw msg;
#else
#define ASSERT(arg)
#define ASSERTMSG(arg, msg)
#endif


/*----------------- INDEX ACCESS ----------------------------*/

    // Index access of an integer vector.
    // Multiple selection allowed: array(1,2,3)[0,0,1] = array(1,1,2)
    RValue * Interpreter::getIntegerElement(RValue *target, RValue *index) {
        ASSERT(target->type == RValueType::Integer);
        ASSERT(index->type == RValueType::Integer);

        int size = index->intVector.size;
        int * vals = new int[size];
        for (int i = 0; i < size; i++) {
            int id = index->intVector.data[i];
            if (id < 0 || static_cast<unsigned>(id) >= target->intVector.size)
                throw InterpreterException("Index outside of a range");
            vals[i] = target->intVector.data[id];
        }
        RValue * res = alloca_->makeIntegerVector(vals, size);
        delete [] vals;
        return res;
    }

    // Index access of a double vector. Multiple selection allowed.
    RValue * Interpreter::getDoubleElement(RValue *target, RValue *index) {
        ASSERT(target->type == RValueType::Double);
        ASSERT(index->type == RValueType::Integer);

        int size = index->intVector.size;
        double * vals = new double[size];
        for (int i = 0; i < size; i++) {
            int id = index->intVector.data[i];
            if (id < 0 || static_cast<unsigned>(id) >= target->dblVector.size)
                throw InterpreterException("Index outside of a range");
            vals[i] = target->dblVector.data[id];
        }
        RValue * res = alloca_->makeDoubleVector(vals, size);
        delete [] vals;
        return res;
    }

    // Index access of character vector
    RValue * Interpreter::getCharacterElement(RValue * target, RValue *index) {
        ASSERT(target->type == RValueType::Character);
        ASSERT(index->type == RValueType::Integer);

        int size = index->intVector.size;
        char * vals = new char[size + 1]; // make room for \0
        for (int i = 0; i < size; i++) {
            int id = index->intVector.data[i];
            if (id < 0 || static_cast<unsigned>(id) >= target->charVector.size)
                throw InterpreterException("Index outside of a range");
            vals[i] = target->charVector.data[id];
        }
        vals[size] = '\0';
        RValue * res = alloca_->makeOrGetCharacterVector(vals);
        delete [] vals;
        return res;
    }



    // Generic index access
    RValue * Interpreter::getElement(RValue *target, RValue *index) {
        if (index->type != RValueType::Integer)
            throw InterpreterException("Error: Only integers can be used as indices");
        switch (target->type) {
        case RValueType::Integer:
            return getIntegerElement(target, index);
        case RValueType::Double:
            return getDoubleElement(target, index);
        case RValueType::Character:
            return getCharacterElement(target, index);
        default:
            throw InterpreterException("Error: Only integers, doubles or characters can be indexed");
        }
        return nullptr;
    }

/*------------------------ CONVERSION TO C++ BOOLEAN TYPE ---------------------------*/

    // Converts runtime value to c++ boolean
    bool Interpreter::toBoolean(RValue *val) {
        // TODO: List all possibilites
        switch (val->type) {
        case RValueType::Character:
            return val->charVector.size > 0;
        case RValueType::Double:
            return val->dblVector.size > 0 && val->dblVector.data[0] != 0;
        case RValueType::Integer:
            return val->intVector.size > 0 && val->intVector.data[0] != 0;
        default:
            ASSERTMSG(0, "Assertion: toBoolean in runtime unhandled case.");
        }
    }

/*------------ BUILT-IN BINARY OPERATIONS -----------------*/

static inline int max(int a, int b) {
    return a > b ? a : b;
}
// NOTE: Consider refactoring to eliminate the huge code repetition among operations.

/* ADDITION */

RValue * Interpreter::addIntegerValues(RValue *lhs, RValue *rhs) {
    int size = max(lhs->intVector.size, rhs->intVector.size);
    RValue * result = alloca_->makeIntegerVectorS(size);
    int ls = lhs->intVector.size; int rs = rhs->intVector.size;
    for (int i = 0; i < size; i++) {
        result->intVector.data[i] = lhs->intVector.data[i % ls] + rhs->intVector.data[i % rs];
    }
    return result;
}

RValue * Interpreter::addDoubleValues(RValue *lhs, RValue *rhs) {
    int size = max(lhs->dblVector.size, rhs->dblVector.size);
    RValue * result = alloca_->makeDoubleVectorS(size);
    int ls = lhs->dblVector.size; int rs = rhs->dblVector.size;
    for (int i = 0; i < size; i++) {
        result->dblVector.data[i] = lhs->dblVector.data[i % ls] + rhs->dblVector.data[i % rs];
    }
    return result;
}

// Adds an integer vector (lhs) and a double vector (rhs) creating a new double vector
RValue * Interpreter::addIntegerAndDoubleValues(RValue *lhs, RValue *rhs) {
    ASSERT(lhs->type == RValueType::Integer && rhs->type == RValueType::Double);
    int size = max(lhs->intVector.size, rhs->dblVector.size);
    int ls = lhs->intVector.size; int rs = rhs->dblVector.size;
    RValue * result = alloca_->makeDoubleVectorS(size);
    for (int i = 0; i < size; i++) {
        result->dblVector.data[i] = lhs->intVector.data[i % ls] + rhs->dblVector.data[i % rs];
    }
    return result;
}

RValue * Interpreter::addCharacterValues(RValue *lhs, RValue *rhs) {
    int size = lhs->charVector.size + rhs->charVector.size;
    char * resultData = new char[size + 1];
    resultData[size] = '\0';
    // Copy first argument
    memcpy(resultData, lhs->charVector.data, lhs->charVector.size);
    // Append the second
    memcpy(resultData + lhs->charVector.size, rhs->charVector.data,
           rhs->charVector.size);
    RValue *result = alloca_->makeOrGetCharacterVector(resultData);
    delete [] resultData;
    return result;
}

RValue * Interpreter::addValues(RValue *lhs, RValue *rhs) {
    switch (lhs->type) {
    case RValueType::Integer:
        switch (rhs->type) {
        case RValueType::Integer:
            return addIntegerValues(lhs, rhs);
        case RValueType::Double:
            return addIntegerAndDoubleValues(lhs, rhs);
        default:
            throw InterpreterException("Incompatible types for addition");
        }

    case RValueType::Double:
        switch (rhs->type) {
        case RValueType::Integer:
            return addIntegerAndDoubleValues(rhs, lhs);
        case RValueType::Double:
            return addDoubleValues(lhs, rhs);
        default:
            throw InterpreterException("Incomatible types for addition");
        }

    case RValueType::Character:
        switch (rhs->type) {
        case RValueType::Character:
            return addCharacterValues(lhs, rhs);
        default:
            throw InterpreterException("Incompatible types for addition");
        }

    default:
        throw InterpreterException("Incompatible types for addition");
    }
}

/* SUBTRACTION */

RValue * Interpreter::subIntegerValues(RValue *lhs, RValue *rhs) {
    int size = max(lhs->intVector.size, rhs->intVector.size);
    RValue * result = alloca_->makeIntegerVectorS(size);
    int ls = lhs->intVector.size; int rs = rhs->intVector.size;
    for (int i = 0; i < size; i++) {
        result->intVector.data[i] = lhs->intVector.data[i % ls] - rhs->intVector.data[i % rs];
    }
    return result;
}

RValue * Interpreter::subDoubleValues(RValue *lhs, RValue *rhs) {
    int size = max(lhs->dblVector.size, rhs->dblVector.size);
    RValue * result = alloca_->makeDoubleVectorS(size);
    int ls = lhs->dblVector.size; int rs = rhs->dblVector.size;
    for (int i = 0; i < size; i++) {
        result->dblVector.data[i] = lhs->dblVector.data[i % ls] - rhs->dblVector.data[i % rs];
    }
    return result;
}

// Adds an integer vector (lhs) and a double vector (rhs) creating a new double vector
RValue * Interpreter::subIntegerAndDoubleValues(RValue *lhs, RValue *rhs) {
    ASSERT( (lhs->type == RValueType::Integer && rhs->type == RValueType::Double)
           || (lhs->type == RValueType::Double && rhs->type == RValueType::Integer));


    if (lhs->type == RValueType::Integer) {
        int size = max(lhs->intVector.size, rhs->dblVector.size);
        int ls = lhs->intVector.size; int rs = rhs->dblVector.size;
        RValue * result = alloca_->makeDoubleVectorS(size);
        for (int i = 0; i < size; i++) {
            result->dblVector.data[i] = lhs->intVector.data[i % ls] - rhs->dblVector.data[i % rs];
        }
        return result;
    } else {
        int size = max(lhs->dblVector.size, rhs->intVector.size);
        int ls = lhs->dblVector.size; int rs = rhs->intVector.size;
        RValue * result = alloca_->makeDoubleVectorS(size);
        for (int i = 0; i < size; i++) {
            result->dblVector.data[i] = lhs->dblVector.data[i % ls] - rhs->intVector.data[i % rs];
        }
        return result;
    }
}

RValue * Interpreter::subValues(RValue *lhs, RValue *rhs) {
    switch (lhs->type) {
    case RValueType::Integer:
        switch (rhs->type) {
        case RValueType::Integer:
            return subIntegerValues(lhs, rhs);
        case RValueType::Double:
            return subIntegerAndDoubleValues(lhs, rhs);
        default:
            throw InterpreterException("Incompatible types for subtraction");
        }

    case RValueType::Double:
        switch (rhs->type) {
        case RValueType::Integer:
            return subIntegerAndDoubleValues(lhs, rhs);
        case RValueType::Double:
            return subDoubleValues(lhs, rhs);
        default:
            throw InterpreterException("Incomatible types for subtraction");
        }
    default:
        throw InterpreterException("Incomatible types for subtraction");
    }
}

/* MULTIPLICATION */

RValue * Interpreter::mulIntegerValues(RValue *lhs, RValue *rhs) {
    int size = max(lhs->intVector.size, rhs->intVector.size);
    RValue * result = alloca_->makeIntegerVectorS(size);
    int ls = lhs->intVector.size; int rs = rhs->intVector.size;
    for (int i = 0; i < size; i++) {
        result->intVector.data[i] = lhs->intVector.data[i % ls] * rhs->intVector.data[i % rs];
    }
    return result;
}

RValue * Interpreter::mulDoubleValues(RValue *lhs, RValue *rhs) {
    int size = max(lhs->dblVector.size, rhs->dblVector.size);
    RValue * result = alloca_->makeDoubleVectorS(size);
    int ls = lhs->dblVector.size; int rs = rhs->dblVector.size;
    for (int i = 0; i < size; i++) {
        result->dblVector.data[i] = lhs->dblVector.data[i % ls] * rhs->dblVector.data[i % rs];
    }
    return result;
}

// Multiplies an integer vector (lhs) and a double vector (rhs) creating a new double vector
RValue * Interpreter::mulIntegerAndDoubleValues(RValue *lhs, RValue *rhs) {
    ASSERT(lhs->type == RValueType::Integer && rhs->type == RValueType::Double);
    int size = max(lhs->intVector.size, rhs->dblVector.size);
    int ls = lhs->intVector.size; int rs = rhs->dblVector.size;
    RValue * result = alloca_->makeDoubleVectorS(size);
    for (int i = 0; i < size; i++) {
        result->dblVector.data[i] = lhs->intVector.data[i % ls] * rhs->dblVector.data[i % rs];
    }
    return result;
}

RValue * Interpreter::mulValues(RValue *lhs, RValue *rhs) {
    switch (lhs->type) {
    case RValueType::Integer:
        switch (rhs->type) {
        case RValueType::Integer:
            return mulIntegerValues(lhs, rhs);
        case RValueType::Double:
            return mulIntegerAndDoubleValues(lhs, rhs);
        default:
            throw InterpreterException("Incompatible types for multiplication");
        }

    case RValueType::Double:
        switch (rhs->type) {
        case RValueType::Integer:
            return mulIntegerAndDoubleValues(rhs, lhs);
        case RValueType::Double:
            return mulDoubleValues(lhs, rhs);
        default:
            throw InterpreterException("Incomatible types for multiplication");
        }
    default:
        throw InterpreterException("Incomatible types for multiplication");
    }
}

/* DIVISION */

RValue * Interpreter::divIntegerValues(RValue *lhs, RValue *rhs) {
    int size = max(lhs->intVector.size, rhs->intVector.size);
    RValue * result = alloca_->makeIntegerVectorS(size);
    int ls = lhs->intVector.size; int rs = rhs->intVector.size;
    for (int i = 0; i < size; i++) {
        if (rhs->intVector.data[i % rs] == 0)
            throw InterpreterException("Zero division!");
        result->intVector.data[i] = lhs->intVector.data[i % ls] / rhs->intVector.data[i % rs];
    }
    return result;
}

RValue * Interpreter::divDoubleValues(RValue *lhs, RValue *rhs) {
    int size = max(lhs->dblVector.size, rhs->dblVector.size);
    RValue * result = alloca_->makeDoubleVectorS(size);
    int ls = lhs->dblVector.size; int rs = rhs->dblVector.size;
    for (int i = 0; i < size; i++) {
        if (rhs->dblVector.data[i % rs] == 0.0)
            throw InterpreterException("Zero division!");
        result->dblVector.data[i] = lhs->dblVector.data[i % ls] / rhs->dblVector.data[i % rs];
    }
    return result;
}

// Divides an integer vector (lhs) and a double vector (rhs) creating a new double vector
RValue * Interpreter::divIntegerAndDoubleValues(RValue *lhs, RValue *rhs) {
    ASSERT((lhs->type == RValueType::Integer && rhs->type == RValueType::Double)
           || (lhs->type == RValueType::Double && rhs->type == RValueType::Integer));
    int size = max(lhs->intVector.size, rhs->dblVector.size);

    if (lhs->type == RValueType::Integer) {
        int ls = lhs->intVector.size; int rs = rhs->dblVector.size;
        RValue * result = alloca_->makeDoubleVectorS(size);
        for (int i = 0; i < size; i++) {
            if (rhs->dblVector.data[i % rs] == 0)
                throw InterpreterException("Zero division!");
            result->dblVector.data[i] = lhs->intVector.data[i % ls] / rhs->dblVector.data[i % rs];
        }
        return result;
    } else {
        int ls = lhs->dblVector.size; int rs = rhs->intVector.size;
        RValue * result = alloca_->makeDoubleVectorS(size);
        for (int i = 0; i < size; i++) {
            if (rhs->intVector.data[i % rs] == 0)
                throw InterpreterException("Zero division!");
            result->dblVector.data[i] = lhs->dblVector.data[i % ls] / rhs->intVector.data[i % rs];
        }
        return result;
    }
}


RValue * Interpreter::divValues(RValue *lhs, RValue *rhs) {
    switch (lhs->type) {
    case RValueType::Integer:
        switch (rhs->type) {
        case RValueType::Integer:
            return divIntegerValues(lhs, rhs);
        case RValueType::Double:
            return divIntegerAndDoubleValues(lhs, rhs);
        default:
            throw InterpreterException("Incompatible types for division");
        }

    case RValueType::Double:
        switch (rhs->type) {
        case RValueType::Integer:
            return divIntegerAndDoubleValues(lhs, rhs);
        case RValueType::Double:
            return divDoubleValues(lhs, rhs);
        default:
            throw InterpreterException("Incomatible types for division");
        }
    default:
        throw InterpreterException("Incomatible types for division");
    }
}

/* LESS THAN */

RValue * Interpreter::ltIntegerValues(RValue *lhs, RValue *rhs) {
    int size = max(lhs->intVector.size, rhs->intVector.size);
    //  Handle size of 1 differently, so cached values might be reused
    if (size == 1)
        return alloca_->makeIntegerVector(lhs->intVector.data[0] < rhs->intVector.data[0]);

    RValue * result = alloca_->makeIntegerVectorS(size);
    int ls = lhs->intVector.size; int rs = rhs->intVector.size;
    for (int i = 0; i < size; i++) {
        result->intVector.data[i] = lhs->intVector.data[i % ls] < rhs->intVector.data[i % rs];
    }
    return result;
}

RValue * Interpreter::ltDoubleValues(RValue *lhs, RValue *rhs) {
    int size = max(lhs->dblVector.size, rhs->dblVector.size);
    RValue * result = alloca_->makeDoubleVectorS(size);
    int ls = lhs->dblVector.size; int rs = rhs->dblVector.size;
    for (int i = 0; i < size; i++) {
        result->dblVector.data[i] = lhs->dblVector.data[i % ls] < rhs->dblVector.data[i % rs];
    }
    return result;
}

// Divides an integer vector (lhs) and a double vector (rhs) creating a new double vector
RValue * Interpreter::ltIntegerAndDoubleValues(RValue *lhs, RValue *rhs) {
    ASSERT((lhs->type == RValueType::Integer && rhs->type == RValueType::Double)
           || (lhs->type == RValueType::Double && rhs->type == RValueType::Integer));
    int size = max(lhs->intVector.size, rhs->dblVector.size);

    if (lhs->type == RValueType::Integer) {
        int ls = lhs->intVector.size; int rs = rhs->dblVector.size;
        RValue * result = alloca_->makeDoubleVectorS(size);
        for (int i = 0; i < size; i++) {
            result->dblVector.data[i] = lhs->intVector.data[i % ls] < rhs->dblVector.data[i % rs];
        }
        return result;
    } else {
        int ls = lhs->dblVector.size; int rs = rhs->intVector.size;
        RValue * result = alloca_->makeDoubleVectorS(size);
        for (int i = 0; i < size; i++) {
            result->dblVector.data[i] = lhs->dblVector.data[i % ls] < rhs->intVector.data[i % rs];
        }
        return result;
    }
}


RValue * Interpreter::ltValues(RValue *lhs, RValue *rhs) {
    switch (lhs->type) {
    case RValueType::Integer:
        switch (rhs->type) {
        case RValueType::Integer:
            return ltIntegerValues(lhs, rhs);
        case RValueType::Double:
            return ltIntegerAndDoubleValues(lhs, rhs);
        default:
            throw InterpreterException("Incompatible types for comparison");
        }

    case RValueType::Double:
        switch (rhs->type) {
        case RValueType::Integer:
            return ltIntegerAndDoubleValues(lhs, rhs);
        case RValueType::Double:
            return ltDoubleValues(lhs, rhs);
        default:
            throw InterpreterException("Incomatible types for comparison");
        }
    default:
        throw InterpreterException("Incomatible types for comparison");
    }
}

/* GREATER THAN */

RValue * Interpreter::gtIntegerValues(RValue *lhs, RValue *rhs) {
    int size = max(lhs->intVector.size, rhs->intVector.size);
    //  Handle size of 1 differently, so cached values might be reused
    if (size == 1)
        return alloca_->makeIntegerVector(lhs->intVector.data[0] > rhs->intVector.data[0]);

    RValue * result = alloca_->makeIntegerVectorS(size);
    int ls = lhs->intVector.size; int rs = rhs->intVector.size;
    for (int i = 0; i < size; i++) {
        result->intVector.data[i] = lhs->intVector.data[i % ls] > rhs->intVector.data[i % rs];
    }
    return result;
}

RValue * Interpreter::gtDoubleValues(RValue *lhs, RValue *rhs) {
    int size = max(lhs->dblVector.size, rhs->dblVector.size);
    RValue * result = alloca_->makeDoubleVectorS(size);
    int ls = lhs->dblVector.size; int rs = rhs->dblVector.size;
    for (int i = 0; i < size; i++) {
        result->dblVector.data[i] = lhs->dblVector.data[i % ls] > rhs->dblVector.data[i % rs];
    }
    return result;
}

// Divides an integer vector (lhs) and a double vector (rhs) creating a new double vector
RValue * Interpreter::gtIntegerAndDoubleValues(RValue *lhs, RValue *rhs) {
    ASSERT((lhs->type == RValueType::Integer && rhs->type == RValueType::Double)
           || (lhs->type == RValueType::Double && rhs->type == RValueType::Integer));
    int size = max(lhs->intVector.size, rhs->dblVector.size);

    if (lhs->type == RValueType::Integer) {
        int ls = lhs->intVector.size; int rs = rhs->dblVector.size;
        RValue * result = alloca_->makeDoubleVectorS(size);
        for (int i = 0; i < size; i++) {
            result->dblVector.data[i] = lhs->intVector.data[i % ls] > rhs->dblVector.data[i % rs];
        }
        return result;
    } else {
        int ls = lhs->dblVector.size; int rs = rhs->intVector.size;
        RValue * result = alloca_->makeDoubleVectorS(size);
        for (int i = 0; i < size; i++) {
            result->dblVector.data[i] = lhs->dblVector.data[i % ls] > rhs->intVector.data[i % rs];
        }
        return result;
    }
}


RValue * Interpreter::gtValues(RValue *lhs, RValue *rhs) {
    switch (lhs->type) {
    case RValueType::Integer:
        switch (rhs->type) {
        case RValueType::Integer:
            return gtIntegerValues(lhs, rhs);
        case RValueType::Double:
            return gtIntegerAndDoubleValues(lhs, rhs);
        default:
            throw InterpreterException("Incompatible types for division");
        }

    case RValueType::Double:
        switch (rhs->type) {
        case RValueType::Integer:
            return gtIntegerAndDoubleValues(lhs, rhs);
        case RValueType::Double:
            return gtDoubleValues(lhs, rhs);
        default:
            throw InterpreterException("Incomatible types for division");
        }
    default:
        throw InterpreterException("Incomatible types for division");
    }
}

/* EQUALS */

RValue * Interpreter::eqIntegerValues(RValue *lhs, RValue *rhs) {
    int size = max(lhs->intVector.size, rhs->intVector.size);
    RValue * result = alloca_->makeIntegerVectorS(size);
    int ls = lhs->intVector.size; int rs = rhs->intVector.size;
    for (int i = 0; i < size; i++) {
        result->intVector.data[i] = lhs->intVector.data[i % ls] == rhs->intVector.data[i % rs];
    }
    return result;
}

RValue * Interpreter::eqDoubleValues(RValue *lhs, RValue *rhs) {
    int size = max(lhs->dblVector.size, rhs->dblVector.size);
    RValue * result = alloca_->makeDoubleVectorS(size);
    int ls = lhs->dblVector.size; int rs = rhs->dblVector.size;
    for (int i = 0; i < size; i++) {
        result->dblVector.data[i] = lhs->dblVector.data[i % ls] == rhs->dblVector.data[i % rs];
    }
    return result;
}

// Adds an integer vector (lhs) and a double vector (rhs) creating a new double vector
RValue * Interpreter::eqIntegerAndDoubleValues(RValue *lhs, RValue *rhs) {
    ASSERT(lhs->type == RValueType::Integer && rhs->type == RValueType::Double);
    int size = max(lhs->intVector.size, rhs->dblVector.size);
    int ls = lhs->intVector.size; int rs = rhs->dblVector.size;
    RValue * result = alloca_->makeDoubleVectorS(size);
    for (int i = 0; i < size; i++) {
        result->dblVector.data[i] = lhs->intVector.data[i % ls] == rhs->dblVector.data[i % rs];
    }
    return result;
}

RValue * Interpreter::eqCharacterValues(RValue *lhs, RValue *rhs) {
    int size = max(lhs->charVector.size, rhs->charVector.size);
    RValue * result = alloca_->makeIntegerVectorS(size);
    int ls = lhs->charVector.size; int rs = rhs->charVector.size;
    for (int i = 0; i < size; i++) {
        result->intVector.data[i] = lhs->charVector.data[i % ls] == rhs->charVector.data[i % rs];
    }
    return result;
}


RValue * Interpreter::eqValues(RValue *lhs, RValue *rhs) {
    switch (lhs->type) {
    case RValueType::Integer:
        switch (rhs->type) {
        case RValueType::Integer:
            return eqIntegerValues(lhs, rhs);
        case RValueType::Double:
            return eqIntegerAndDoubleValues(lhs, rhs);
        default:
            throw InterpreterException("Incompatible types for addition");
        }

    case RValueType::Double:
        switch (rhs->type) {
        case RValueType::Integer:
            return eqIntegerAndDoubleValues(rhs, lhs);
        case RValueType::Double:
            return eqDoubleValues(lhs, rhs);
        default:
            throw InterpreterException("Incomatible types for addition");
        }

    case RValueType::Character:
        switch (rhs->type) {
        case RValueType::Character:
            return eqCharacterValues(lhs, rhs);
        default:
            throw InterpreterException("Incompatible types for addition");
        }

    default:
        throw InterpreterException("Incompatible types for addition");
    }
}

/* NOT EQUAL */

RValue * Interpreter::neqIntegerValues(RValue *lhs, RValue *rhs) {
    int size = max(lhs->intVector.size, rhs->intVector.size);
    RValue * result = alloca_->makeIntegerVectorS(size);
    int ls = lhs->intVector.size; int rs = rhs->intVector.size;
    for (int i = 0; i < size; i++) {
        result->intVector.data[i] = lhs->intVector.data[i % ls] != rhs->intVector.data[i % rs];
    }
    return result;
}

RValue * Interpreter::neqDoubleValues(RValue *lhs, RValue *rhs) {
    int size = max(lhs->dblVector.size, rhs->dblVector.size);
    RValue * result = alloca_->makeDoubleVectorS(size);
    int ls = lhs->dblVector.size; int rs = rhs->dblVector.size;
    for (int i = 0; i < size; i++) {
        result->dblVector.data[i] = lhs->dblVector.data[i % ls] != rhs->dblVector.data[i % rs];
    }
    return result;
}

// Adds an integer vector (lhs) and a double vector (rhs) creating a new double vector
RValue * Interpreter::neqIntegerAndDoubleValues(RValue *lhs, RValue *rhs) {
    ASSERT(lhs->type == RValueType::Integer && rhs->type == RValueType::Double);
    int size = max(lhs->intVector.size, rhs->dblVector.size);
    int ls = lhs->intVector.size; int rs = rhs->dblVector.size;
    RValue * result = alloca_->makeDoubleVectorS(size);
    for (int i = 0; i < size; i++) {
        result->dblVector.data[i] = lhs->intVector.data[i % ls] != rhs->dblVector.data[i % rs];
    }
    return result;
}

RValue * Interpreter::neqCharacterValues(RValue *lhs, RValue *rhs) {
    int size = max(lhs->charVector.size, rhs->charVector.size);
    RValue * result = alloca_->makeIntegerVectorS(size);
    int ls = lhs->charVector.size; int rs = rhs->charVector.size;
    for (int i = 0; i < size; i++) {
        result->intVector.data[i] = lhs->charVector.data[i % ls] != rhs->charVector.data[i % rs];
    }
    return result;
}


RValue * Interpreter::neqValues(RValue *lhs, RValue *rhs) {
    switch (lhs->type) {
    case RValueType::Integer:
        switch (rhs->type) {
        case RValueType::Integer:
            return neqIntegerValues(lhs, rhs);
        case RValueType::Double:
            return neqIntegerAndDoubleValues(lhs, rhs);
        default:
            throw InterpreterException("Incompatible types for addition");
        }

    case RValueType::Double:
        switch (rhs->type) {
        case RValueType::Integer:
            return neqIntegerAndDoubleValues(rhs, lhs);
        case RValueType::Double:
            return neqDoubleValues(lhs, rhs);
        default:
            throw InterpreterException("Incomatible types for addition");
        }

    case RValueType::Character:
        switch (rhs->type) {
        case RValueType::Character:
            return neqCharacterValues(lhs, rhs);
        default:
            throw InterpreterException("Incompatible types for addition");
        }

    default:
        throw InterpreterException("Incompatible types for addition");
    }
}

/*---------------------- BUILT-IN CALLS ---------------------------*/


RValue * Interpreter::makeArray(std::vector<RValue *> &args) {
    if (args.empty())
        return alloca_->getNilSingletion();

    RValueType type = args[0]->type;
    bool mixed = false;
    for (size_t i = 1; i < args.size(); i++) {
        if (args[i]->type != type) { // Trying to create non homogenous array
            if (type == RValueType::Integer || type == RValueType::Double) {
                // Only allow mix of doubles and integers
                for (size_t j = i; j < args.size(); j++) {
                    if (args[j]->type != RValueType::Integer && args[j]->type != RValueType::Double)
                        throw InterpreterException("Only homogenous arrays are allowed");
                }
                mixed = true;
                break;
            } else
                throw InterpreterException("Only homogenous arrays are allowed");
        }
    }

    // Arrays from mixed integers and doubles. Integers will be casted to doubles.
    if (mixed) {
        int size = 0;
        for (RValue * arg : args) {
            if (arg->type == RValueType::Integer)
                size += arg->intVector.size;
            else
                size += arg->dblVector.size;
        }
        RValue * result = alloca_->makeDoubleVectorS(size);
        int offset = 0;
        for (RValue * arg: args) {
            if (arg->type == RValueType::Integer) {
                for (uint32_t i = 0; i < arg->intVector.size; i++) {
                    result->dblVector.data[offset + i] = arg->intVector.data[i];                    
                }
            offset += arg->intVector.size;
            }
            else {
                for (uint32_t i = 0; i < arg->dblVector.size; i++) {
                    result->dblVector.data[offset + i] = arg->dblVector.data[i];
                }
                offset += arg->dblVector.size;
            }
        }
        return result;
    }

    // Homogenous arrays
    switch (type) {
    case RValueType::Integer: { // Integer + Integer
        int size = 0;
        for (RValue * arg : args) {
            size += arg->intVector.size;
        }
        RValue * result = alloca_->makeIntegerVectorS(size);
        int offset = 0;
        for (RValue * arg: args) {
            memcpy(result->intVector.data + offset, arg->intVector.data, arg->intVector.size * sizeof(int));
            offset += arg->intVector.size;
        }
        return result;
    }

    case RValueType::Double: { // Double + Double
        int size = 0;
        for (RValue * arg : args) {
            size += arg->dblVector.size;
        }
        RValue * result = alloca_->makeDoubleVectorS(size);
        int offset = 0;
        for (RValue * arg: args) {
            memcpy(result->dblVector.data + offset, arg->dblVector.data, arg->dblVector.size * sizeof(double));
            offset += arg->dblVector.size;
        }
        return result;
    }

    case RValueType::Character: { // Character + Character
        int size = 0;
        for (RValue * arg : args) {
            size += arg->charVector.size;
        }
        char * resultData = new char(size + 1);
        resultData[size] = '\0';
        int offset = 0;
        for (RValue * arg: args) {
            memcpy(resultData + offset, arg->charVector.data, arg->charVector.size * sizeof(char));
            offset += arg->charVector.size;
        }
        RValue * result = alloca_->makeOrGetCharacterVector(resultData);
        return result;
    }
    default:
        throw InterpreterException("Only arrays of number or characters are valid.");
    }
}

int Interpreter::length(RValue *arg) {
    switch (arg->type) {
    case RValueType::Character:
        return arg->charVector.size;
    case RValueType::Double:
        return arg->dblVector.size;
    case RValueType::Integer:
        return arg->intVector.size;
    case RValueType::Nil:
        return 0;
    default:
        throw InterpreterException("Can only determine size of number or char vectors.");
    }
}

/* Load file call */
#ifndef CONTPASS
void Interpreter::load(RValue *arg) {
    if (arg->type != RValueType::Character)
        throw InterpreterException("Load expects a character vector as argument.");
    std::ifstream input;
    input.open(arg->charVector.data);
    if (!input.is_open())
        throw InterpreterException("Error while trying to load the specified file.");

    std::unique_ptr<ast::Function> result;
    try {
        result.reset(Parser::parseInput(input));
    } catch (ParserException &pe) {
        input.close();
        throw InterpreterException("Error when parsing loaded file.");
    }
    input.close();

    ast::Function * parsedCode = result.release();
    loadStack_.push_back(parsedCode); // Taking ownership of the parsed code
    parsedCode->body->processBy(this);
    loadStack_.pop_back();
    delete parsedCode;
}
#else
void Interpreter::contLoadCall2(TrampolineData &data) {
    // Release the arg
    RValue * evalArg = popAndRetTemp();
    if (evalArg->type != RValueType::Character)
        throw InterpreterException("Load expects a character vector as argument.");
    std::ifstream input;
    input.open(evalArg->charVector.data);
    if (!input.is_open())
        throw InterpreterException("Error while trying to load the specified file.");

    std::unique_ptr<ast::Function> result;
    try {
        result.reset(Parser::parseInput(input));
    } catch (ParserException &pe) {
        input.close();
        throw InterpreterException("Error when parsing loaded file.");
    }
    input.close();
    loadStack_.push_back(result.release()); // Taking ownership of the parsed code

    TrampolineData arg;
    arg.command = Command::Call;
    arg.code = &Interpreter::contLoadCall3;
    arg.argLoadCall = data.argLoadCall;
    programStack.push(arg);

    // Eval the parsed file
    loadStack_.back()->body->processBy(this);
}
#endif

/*---------------------------- INDEX ASSIGNMENTS ----------------------------------*/

RValue * Interpreter::indexDoubleAssignment(RValue * target, RValue * index, RValue * rhs) {
    ASSERT(target->type == RValueType::Double && index->type == RValueType::Integer
           && rhs->type == target->type);

    for (uint32_t i = 0; i < index->intVector.size; i++) {
        int id = index->intVector.data[i];
        if (id < 0 || static_cast<unsigned>(id) > target->dblVector.size)
            throw InterpreterException("Index outside of a bound");
        target->dblVector.data[id] = rhs->dblVector.data[i % rhs->dblVector.size];
    }
    return target;
}

RValue * Interpreter::indexIntegerAssignment(RValue * target, RValue * index, RValue * rhs) {
    ASSERT(target->type == RValueType::Integer && index->type == RValueType::Integer
           && rhs->type == target->type);

    for (uint32_t i = 0; i < index->intVector.size; i++) {
        int id = index->intVector.data[i];
        if (id < 0 || static_cast<unsigned>(id) > target->intVector.size)
            throw InterpreterException("Index outside of a bound");
        target->intVector.data[id] = rhs->intVector.data[i % rhs->intVector.size];
    }
    return target;
}

RValue * Interpreter::indexCharacterAssignment(RValue * target, RValue * index, RValue * rhs) {
    ASSERT(target->type == RValueType::Character && index->type == RValueType::Integer
           && rhs->type == target->type);

    for (uint32_t i = 0; i < index->intVector.size; i++) {
        int id = index->intVector.data[i];
        if (id < 0 || static_cast<unsigned>(id) > target->charVector.size)
            throw InterpreterException("Index outside of a bound");
        target->charVector.data[id] = rhs->charVector.data[i % rhs->charVector.size];
    }
    return target;
}

RValue * Interpreter::indexIntToDoubleAssignment(RValue * target, RValue * index, RValue * rhs) {
    ASSERT(target->type == RValueType::Double && index->type == RValueType::Integer
           && rhs->type == RValueType::Integer);

    for (uint32_t i = 0; i < index->intVector.size; i++) {
        int id = index->intVector.data[i];
        if (id < 0 || static_cast<unsigned>(id) > target->dblVector.size)
            throw InterpreterException("Index outside of a bound");
        target->dblVector.data[id] = rhs->intVector.data[i % rhs->charVector.size];
    }
    return target;
}

RValue * Interpreter::indexAssignment(RValue * target, RValue * index, RValue * rhs, RValue* targetId) {
    if (index->type != RValueType::Integer)
        throw InterpreterException("Only integer indices are allowed.");

    // This is a mutable assignment, we need to apply copy on write if the value is shared
    if (target->shareLinks > 1 || alloca_->isSingleton(target)) {
        RValue *old = target;
        switch (target->type) {
        case RValueType::Double:
            target = alloca_->makeDoubleVectorS(old->dblVector.size);
            memcpy(target->dblVector.data, old->dblVector.data, old->dblVector.size *sizeof(double));
            if (targetId != nullptr)
                env_->set(targetId, target); // change the binding onto the copied value
            break;
        case RValueType::Integer:
            target = alloca_->makeIntegerVectorS(old->intVector.size);
            memcpy(target->intVector.data, old->intVector.data, old->intVector.size *sizeof(int));
            if (targetId != nullptr)
                env_->set(targetId, target); // change the binding onto the copied value
            break;
        case RValueType::Character:
            target = alloca_->makeCharacterVectorS(old->charVector.size);
            memcpy(target->charVector.data, old->charVector.data, old->charVector.size *sizeof(char));
            if (targetId != nullptr)
                env_->set(targetId, target); // change the binding onto the copied value
            break;
        default:
            break;
        }
    }

    switch (target->type) {
    case RValueType::Double:
        if (rhs->type == RValueType::Double)
            return indexDoubleAssignment(target, index, rhs);
        else if (rhs->type == RValueType::Integer)
            return indexIntToDoubleAssignment(target, index, rhs);
        throw InterpreterException("Only double or integers can be assigned to doubles");
    case RValueType::Integer:
        if (rhs->type == RValueType::Integer)
            return indexIntegerAssignment(target, index, rhs);
        else if (rhs->type == RValueType::Double)
            throw InterpreterException("Cannot upcast integer to double in a mutable operation.");
        throw InterpreterException("Only integers can be assigned to integers.");
    case RValueType::Character:
        if (rhs->type == RValueType::Character)
            return indexCharacterAssignment(target, index, rhs);
        throw InterpreterException("Only characters can be assigned to characters");
    default:
       throw InterpreterException("Only numbers or string literals can be indexed");
    }
}

}
