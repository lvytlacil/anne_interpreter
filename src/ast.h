#ifndef AST_H
#define AST_H

#include <ostream>
#include <vector>
class Symbol;

class AstProcessor;
namespace pool {
    class Stringpool;
}

#define DELPTR(ptr) if (ptr != nullptr && ptr->canBeDeleted()) delete ptr;

namespace ast {

/**
  The following are definitions of different AST nodes. Notice, that most of them
  have constructors, that do not initialize all of their members. This is done on purpose --
  the parser, that creates their objects works sequentially and it is convinient to actually
  be able to create a node, before all its members are parsed and then set such members later, once
  they are known.
  */

/**
 * @brief Represents an arbitrary expression
 */
class Function;

class Expression {
public:
    bool isTailExpression; // Determined at runtime (sound, but far from full)
    virtual bool canBeDeleted() { return true; }
    Expression():isTailExpression(false) { }
    virtual ~Expression() { }
    virtual void processBy(AstProcessor * proc) = 0;
    void dump(std::ostream &out);
};


/**
 * @brief Represents an integer literal
 */
class IntNumber : public Expression {
public:
    int value;

    IntNumber(int parsedValue): value(parsedValue) { }

    void processBy(AstProcessor *proc) override;
};

/**
 * @brief Represents a double literal
 */
class RealNumber : public Expression {
public:
    double value;

    RealNumber(double parsedValue): value(parsedValue) { }

    void processBy(AstProcessor *proc) override;
};

class Function;
/**
 * @brief Represents a string literal
 */
class StringLit : public Expression {
private:
    Symbol * symbol; // The pool manages these objects, don't delete itself.
public:
    Function * parentFunction; // To access parent function's string pool

    StringLit(Function * parentFun, Symbol *symbol):
        symbol(symbol),
        parentFunction(parentFun) { }

    const char * getName();

    void processBy(AstProcessor *proc) override;
};

/**
 * @brief Represents an identifier.
 */
class Identifier : public Expression {
private:
    Symbol * symbol; // The pool manages these objects, don't delete itself.
public:
    Function * parentFunction; // To access parent function's string pool
    Identifier(Function * parentFun, Symbol *symbol):
        symbol(symbol),
        parentFunction(parentFun) { }

    const char * getName();

    void processBy(AstProcessor *proc) override;


};

/**
 * @brief Represents a sequence of statements.
 */
class Sequence : public Expression {
public:
    std::vector<Expression*> body;

    ~Sequence() override {
        for (Expression * exp : body)
            DELPTR(exp);
    }

    void processBy(AstProcessor *proc) override;
};

/**
 * @brief Represents a function definition.
 */
class Function : public Expression {
public:
    pool::Stringpool *pool_;
    Sequence *body;
    Function *parent;
    std::vector<Identifier*> args;
    int links; // No. links at this function definition at runtime
    std::vector<Function*> children; // Direct child functions
    // If true, body won't be deleted when deleting this node
    // This is used, when runtime function gets created and actually needs its body stored
    // to be reinterpred in different contexts when calling that function.
    //bool claimed;

    Function(Sequence *funcBody, Function* funcParent):
        body(funcBody), parent(funcParent), links(0) { }
    Function(Sequence *funcBody):
        body(funcBody), parent(nullptr), links(0) { }

    Function():
        body(nullptr), parent(nullptr), links(0) { }

    ~Function() override;
    bool canBeDeleted() override {
        return links <= 0;
    }
    void processBy(AstProcessor *proc) override;

};



/**
 * @brief Represents a binary expression node.
 */
class BinaryExp : public Expression {
public:
    enum class OpCode {
        add,
        sub,
        mul,
        div,
        eq,
        neq,
        lt,
        gt,
    };

    Expression *lhs;
    Expression *rhs;
    OpCode opCode;

    BinaryExp(Expression *lhsExp, Expression *rhsExp, OpCode op):
        lhs(lhsExp), rhs(rhsExp), opCode(op) { }

    ~BinaryExp() override {
        DELPTR(lhs);
        DELPTR(rhs);
    }

    void processBy(AstProcessor * proc) override;
};

/**
 * @brief Represents a user defined call node
 */
class Call final : public Expression {
public:
    Expression *callee;
    std::vector<Expression *> args;

    Call(Expression *calleeExp): callee(calleeExp) { }

    ~Call() override {
        for (Expression *arg : args)
            DELPTR(arg);
        DELPTR(callee);
    }

    void processBy(AstProcessor *proc) override;
};



/**
 * @brief Represents a built-in call that creates an array
 */
class ArrayCall final : public Expression {
public:    
    std::vector<Expression *> args;

    ~ArrayCall() override {
        for (Expression *arg : args)
            DELPTR(arg);
    }
    void processBy(AstProcessor *proc) override;
};

class LenCall final : public Expression {
public:
    Expression *arg;
    void processBy(AstProcessor *proc) override;
    ~LenCall() override {
        DELPTR(arg);
    }
};

class LoadCall final : public Expression {
public:
    Expression *arg;
    void processBy(AstProcessor *proc) override;
    ~LoadCall() override {
        DELPTR(arg);
    }
};

class TypeCall final : public Expression {
public:
    Expression *arg;
    void processBy(AstProcessor *proc) override;
    ~TypeCall() override {
        DELPTR(arg);
    }
};

/**
 * @brief Represents a index-access expresssion
 */
class Index : public Expression {
public:
    Expression *index;
    Expression *identifier;

    Index(Expression *identifierExp):
        identifier(identifierExp) { }

    ~Index() override {
        DELPTR(index);
        DELPTR(identifier);
    }

    void processBy(AstProcessor *proc) override;
};

/**
 * @brief Represents a simple assignment expression.
 */
class Assignment : public Expression {
public:
    Identifier *dest;
    Expression *rhs;

    Assignment(Identifier * destExp):
        dest(destExp) { }

    ~Assignment() override {
        DELPTR(dest);
        DELPTR(rhs);
    }

    void processBy(AstProcessor *proc) override;
};

/**
 * @brief Represents an assignment to an index-access
 */
class IndexedAssignment : public Expression {
public:
    Index *dest;
    Expression *rhs;

    IndexedAssignment(Index *destExp):
        dest(destExp) { }

    ~IndexedAssignment() override {
        DELPTR(dest);
        DELPTR(rhs);
    }

    void processBy(AstProcessor *proc) override;
};

/**
 * @brief Represents a classic if-then-else conditional expression.
 */
class ConditionalExp : public Expression {
public:
    Expression *cond;
    Expression *trueCase;
    Expression *falseCase;

    ConditionalExp(Expression *condExp):
        cond(condExp) { }

    ~ConditionalExp() override {
        DELPTR(cond);
        DELPTR(trueCase);
        DELPTR(falseCase);
    }

    void processBy(AstProcessor *proc) override;
};

/**
 * @brief Represents a classic while loop expression.
 */
class LoopExp : public Expression {
public:
    Expression *cond;
    Expression *body;

    LoopExp(Expression *condExp):
        cond(condExp) { }

    ~LoopExp() override {
        DELPTR(cond);
        DELPTR(body);
    }

    void processBy(AstProcessor *proc) override;
};


}


#endif // AST_H
