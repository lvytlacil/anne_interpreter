#ifndef INTERPRETER_H
#define INTERPRETER_H

#include "astprocessor.h"
#include "allocator.h"
#include "symboltable.h"
#include <iostream>
#include <stack>

#ifdef CONTPASS
#include <memory>
#endif

namespace interpreter {



class Interpreter final : public AstProcessor
{
private:
    RValue *envHolder_; // Holds the environment
    Environment *env_; // Convinience for extracting the actual environment from the holder
    RValue *result_; // Interpretation result
    Allocator *alloca_; // Underlying allocator

    Interpreter() { }

    void changeEnvironment(RValue *newEnv) {
        envHolder_ = newEnv;
        env_ = envHolder_->environment.env;
        alloca_->setEnvironmentRoot(envHolder_);
    }

    // Used to protect temporary values from GC
    std::vector<RValue*> temporaries;

    // Stores loaded and parsed code (via load call) to clean it up properly
    std::vector<ast::Function*> loadStack_;

    /* Temporaries stack manipulation */
    inline void  popTemp() {
        temporaries.pop_back();
    }

    inline RValue * popAndRetTemp() {
        RValue * res = temporaries.back();
        temporaries.pop_back();
        return res;
    }

    inline RValue * getTempFromTop(int offset) {
        return temporaries[temporaries.size() - offset - 1];
    }

    inline void pushTemp(RValue *val) {
        temporaries.push_back(val);
    }

    /* Runtime operations declarations */
    RValue * getIntegerElement(RValue *target, RValue *index);
    RValue * getDoubleElement(RValue *target, RValue *index);
    RValue * getCharacterElement(RValue * target, RValue *index);
    RValue * getElement(RValue *target, RValue *index);
    bool toBoolean(RValue *val);
    RValue * addIntegerValues(RValue *lhs, RValue *rhs);
    RValue * addDoubleValues(RValue *lhs, RValue *rhs);
    RValue * addIntegerAndDoubleValues(RValue *lhs, RValue *rhs);
    RValue * addCharacterValues(RValue *lhs, RValue *rhs);
    RValue * addValues(RValue *lhs, RValue *rhs);
    RValue * subIntegerValues(RValue *lhs, RValue *rhs);
    RValue * subDoubleValues(RValue *lhs, RValue *rhs);
    RValue * subIntegerAndDoubleValues(RValue *lhs, RValue *rhs);
    RValue * subValues(RValue *lhs, RValue *rhs);
    RValue * mulIntegerValues(RValue *lhs, RValue *rhs);
    RValue * mulDoubleValues(RValue *lhs, RValue *rhs);
    RValue * mulIntegerAndDoubleValues(RValue *lhs, RValue *rhs);
    RValue * mulValues(RValue *lhs, RValue *rhs);
    RValue * divIntegerValues(RValue *lhs, RValue *rhs);
    RValue * divDoubleValues(RValue *lhs, RValue *rhs);
    RValue * divIntegerAndDoubleValues(RValue *lhs, RValue *rhs);
    RValue * divValues(RValue *lhs, RValue *rhs);
    RValue * ltIntegerValues(RValue *lhs, RValue *rhs);
    RValue * ltDoubleValues(RValue *lhs, RValue *rhs);
    RValue * ltIntegerAndDoubleValues(RValue *lhs, RValue *rhs);
    RValue * ltValues(RValue *lhs, RValue *rhs);
    RValue * gtIntegerValues(RValue *lhs, RValue *rhs);
    RValue * gtDoubleValues(RValue *lhs, RValue *rhs);
    RValue * gtIntegerAndDoubleValues(RValue *lhs, RValue *rhs);
    RValue * gtValues(RValue *lhs, RValue *rhs);
    RValue * eqValues(RValue *lhs, RValue *rhs);
    RValue * eqDoubleValues(RValue *lhs, RValue *rhs);
    RValue * eqIntegerValues(RValue *lhs, RValue *rhs);
    RValue * eqCharacterValues(RValue *lhs, RValue *rhs);
    RValue * eqIntegerAndDoubleValues(RValue *lhs, RValue *rhs);
    RValue * neqValues(RValue *lhs, RValue *rhs);
    RValue * neqDoubleValues(RValue *lhs, RValue *rhs);
    RValue * neqIntegerValues(RValue *lhs, RValue *rhs);
    RValue * neqCharacterValues(RValue *lhs, RValue *rhs);
    RValue * neqIntegerAndDoubleValues(RValue *lhs, RValue *rhs);
    RValue * makeArray(std::vector<RValue *> &args);
    RValue * indexDoubleAssignment(RValue * target, RValue * index, RValue * rhs);
    RValue * indexIntegerAssignment(RValue * target, RValue * index, RValue * rhs);
    RValue * indexCharacterAssignment(RValue * target, RValue * index, RValue * rhs);
    RValue * indexIntToDoubleAssignment(RValue * target, RValue * index, RValue * rhs);
    RValue * indexAssignment(RValue * target, RValue * index, RValue * rhs, RValue *targetId);
    int length(RValue * arg);
    void load(RValue *arg);


    Interpreter(ast::Sequence* program, RValue *envHolder, Allocator *alloca):
        alloca_(alloca) {
        changeEnvironment(envHolder);
        result_ = nullptr;
        alloca->setTemporaryStack(temporaries);
#ifdef CONTPASS
        initPushPopTrampolineData();
#endif
    }
public:
    /**
     * @brief Starts an interpretation of the program within the given environment and under
     * the given allocator.
     * @param program
     * @param envHolder
     * @param alloca
     * @return The result of the interpretation as a runtime value.
     */
    static RValue * interpret(ast::Sequence* program, RValue *envHolder, Allocator *alloca) {
        Interpreter in(program, envHolder, alloca);

        try {
#ifdef CONTPASS
            in.process(program);
            in.trampoline();
#else
            program->processBy(&in);
#endif
        } catch (InterpreterException &ie) {
            for (ast::Function * code : in.loadStack_)
                delete code;
            throw ie;
        }

        for (ast::Function * code : in.loadStack_)
            delete code;

        return in.result_ == nullptr ? alloca->makeIntegerVector(0) : in.result_;
    }

#ifndef CONTPASS
public:
    /**
     * @brief Interprets the given sequence of statements in the given environment, using the given
     * symbol table
     * @param program
     * @param env
     * @param symbolTable
     * @return The final interpreted value
     */


    virtual void process(ast::Expression * node) { }
    virtual void process(ast::Assignment * node) {
        node->rhs->processBy(this);
        pushTemp(result_); // protect rhs
        env_->set(alloca_->makeOrGetCharacterVector(node->dest->getName()), result_);
        popTemp();
    }
    virtual void process(ast::BinaryExp * node) {
        node->lhs->processBy(this);
        pushTemp(result_); // Protect lhs
        RValue *lhs = result_;
        node->rhs->processBy(this);
        pushTemp(result_); // Protect rhs
        RValue *rhs = result_;

        switch (node->opCode) {
        case ast::BinaryExp::OpCode::add:
            result_ = addValues(lhs, rhs);
            break;
        case ast::BinaryExp::OpCode::sub:
            result_ = subValues(lhs, rhs);
            break;
        case ast::BinaryExp::OpCode::mul:
            result_ = mulValues(lhs, rhs);
            break;
        case ast::BinaryExp::OpCode::div:
            result_ = divValues(lhs, rhs);
            break;
        case ast::BinaryExp::OpCode::lt:
            result_ = ltValues(lhs, rhs);
            break;
        case ast::BinaryExp::OpCode::gt:
            result_ = gtValues(lhs, rhs);
            break;
        case ast::BinaryExp::OpCode::eq:
            result_ = eqValues(lhs, rhs);
            break;
        case ast::BinaryExp::OpCode::neq:
            result_ = neqValues(lhs, rhs);
            break;
        default:
            throw InterpreterException("BinOp Not implemented");
        }
        popTemp(); popTemp();
    }
    virtual void process(ast::Call * node) {
        // Eval callee
        node->callee->processBy(this);
        pushTemp(result_); // Protect
        RValue * func = result_;

        // Eval args.
        std::vector<RValue *> args;
        for (ast::Expression *arg : node->args) {
            arg->processBy(this);
            args.push_back(result_);
            pushTemp(result_); // protect each arg
        }


        if (func->type != RValueType::FunctionDef)
            throw InterpreterException("Only functions may be called");
        FunctionDefinition & fdef = func->funcDef; // Reference must be taken !!!
        if (fdef.argCount != args.size())
            throw InterpreterException("Incorrect number of arguments passed");

        /* We need to make a new local environment or reuse the old one, if
         * the same function as this environment was created for is called again */
        RValue * localEnvHolder = alloca_->makeLocalEnvironment(fdef.envHolder);

        // Make bindings from actual arguments to formal parameters and release them

        for (int i = args.size() - 1; i >= 0; i--) {
            RValue *key = fdef.formalArgs[i];
            RValue *val = popAndRetTemp();
            localEnvHolder->environment.env->set(key, val);
        }

        // Save old one and change into the new one
        pushTemp(envHolder_);
        changeEnvironment(localEnvHolder);

        // Evaluate the function body. Then restore the old environment. Release callee.
        fdef.code->body->processBy(this);
        changeEnvironment(popAndRetTemp());
        popTemp();
    }

    virtual void process(ast::ArrayCall * node) {
        std::vector<RValue *> args;
        for (ast::Expression *arg : node->args) {
            arg->processBy(this);
            args.push_back(result_);
            pushTemp(result_); // protect each arg
        }
        result_ = makeArray(args);
        // Release arguments
        for (size_t i = 0; i < args.size(); i++) {
            popTemp();
        }
    }

    virtual void process(ast::LenCall * node) {
        node->arg->processBy(this);
        result_ = alloca_->makeIntegerVector(length(result_));
    }

    virtual void process(ast::LoadCall * node) {
        node->arg->processBy(this);
        load(result_);
    }

    virtual void process(ast::TypeCall * node) {
        node->arg->processBy(this);
        result_ = alloca_->makeOrGetCharacterVector(RValue::typeToString(result_->type));
    }

    virtual void process(ast::ConditionalExp * node) {
        node->cond->processBy(this);
        if (toBoolean(result_))
            node->trueCase->processBy(this);
        else
            node->falseCase->processBy(this);
    }

    virtual void process(ast::Function * node) {
        std::vector<RValue*> evalArgs;
        for (ast::Identifier * arg : node->args) {
            result_ = alloca_->makeOrGetCharacterVector(arg->getName());
            pushTemp(result_); // protect
            evalArgs.push_back(result_);
        }

        result_ = alloca_->makeFunctionDefinition(node, envHolder_, node->args.size());
        node->links++; // New runtime link created
        // Release arguments and store them to the runtime object function
        for (int i = node->args.size() - 1; i >= 0; i--) {
            result_->funcDef.formalArgs[i] = popAndRetTemp();
        }
    }

    virtual void process(ast::Identifier * node) {
        result_ = env_->get(alloca_->makeOrGetCharacterVector(node->getName()));
    }

    virtual void process(ast::Index * node) {
        node->identifier->processBy(this);
        pushTemp(result_); // protect
        RValue *target = result_;
        node->index->processBy(this);
        pushTemp(result_); // protect
        RValue *index = result_;
        result_ = getElement(target, index);
        // Release
        popTemp(); popTemp();
    }

    virtual void process(ast::IndexedAssignment * node) {
        node->rhs->processBy(this);
        pushTemp(result_); // protect
        RValue * rhs = result_;
        node->dest->identifier->processBy(this);
        pushTemp(result_); // protect
        RValue * target = result_;
        node->dest->index->processBy(this);
        pushTemp(result_); // protect
        RValue * index = result_;

        ast::Identifier *targetIDNode = dynamic_cast<ast::Identifier*>(node->dest->identifier);
        RValue *targetID = nullptr;
        if (targetIDNode != nullptr)
            targetID = alloca_->makeOrGetCharacterVector(targetIDNode->getName());
        pushTemp(targetID);

        // We can either return the right hand side, or the target. It depends
        // on what behaviour we want.. I choose returning right hand side for example.
        indexAssignment(target, index, rhs, targetID);
        result_ = rhs;
        // Release
        popTemp(); popTemp(); popTemp(); popTemp();
    }

    virtual void process(ast::IntNumber * node) {
        result_ = alloca_->makeIntegerVector(node->value);
    }
    virtual void process(ast::LoopExp * node) {
        while (true) {
            node->cond->processBy(this);
            if (!toBoolean(result_))
                break;
            node->body->processBy(this);
        }
    }
    virtual void process(ast::RealNumber * node) {
        result_ = alloca_->makeDoubleVector(node->value);
    }

    virtual void process(ast::Sequence * node) {
        if (node->body.empty())
            result_ = alloca_->makeIntegerVector(0);
        for (ast::Expression *exp : node->body) {
            exp->processBy(this);
        }
    }

    virtual void process(ast::StringLit * node) {
        result_ = alloca_->makeOrGetCharacterVector(node->getName());
    }

    /*-------------------------------------------------------------------------------------------------*/
    /*--------------------------------------------------------------------------------------------------*/
    /*-----------------------------------------------------------------------------------------------------*/
#else
    /* AST Processor implementation methods just create frames for continuation passing and return to trampoline */
    virtual void process(ast::Expression * node) { }
    virtual void process(ast::Assignment * node) {
        TrampolineData arg;
        arg.command = Command::Call;
        arg.code = &Interpreter::contAssignment;
        arg.argAssignment = node;
        programStack.push(arg);
    }
    virtual void process(ast::BinaryExp * node) {
        TrampolineData arg;
        arg.command = Command::Call;
        arg.code = &Interpreter::contBinaryExp;
        arg.argBinaryExp = node;
        programStack.push(arg);
    }
    virtual void process(ast::Call * node) {
        TrampolineData arg;
        arg.command = Command::Call;
        arg.code = &Interpreter::contCall;
        arg.argCall = node;
        programStack.push(arg);
    }

    virtual void process(ast::ArrayCall * node) {
        TrampolineData arg;
        arg.command = Command::Call;
        arg.code = &Interpreter::contArrayCall;
        arg.argArrayCall = node;
        programStack.push(arg);
    }
    virtual void process(ast::ConditionalExp * node) {
        TrampolineData arg;
        arg.command = Command::Call;
        arg.code = &Interpreter::contConditionalExp;
        arg.argConditionalExp = node;
        programStack.push(arg);
    }
    virtual void process(ast::Function * node) {
        TrampolineData arg;
        arg.command = Command::Call;
        arg.code = &Interpreter::contFunction;
        arg.argFunction = node;
        programStack.push(arg);
    }
    virtual void process(ast::Identifier * node) {
        TrampolineData arg;
        arg.command = Command::Call;
        arg.code = &Interpreter::contIdentifier;
        arg.argIdentifier = node;
        programStack.push(arg);
    }
    virtual void process(ast::Index * node) {
        TrampolineData arg;
        arg.command = Command::Call;
        arg.code = &Interpreter::contIndex;
        arg.argIndex = node;
        programStack.push(arg);
    }
    virtual void process(ast::IndexedAssignment * node) {
        TrampolineData arg;
        arg.command = Command::Call;
        arg.code = &Interpreter::contIndexAssignment;
        arg.argIndexAssignment = node;
        programStack.push(arg);
    }
    virtual void process(ast::IntNumber * node) {
        TrampolineData arg;
        arg.command = Command::Call;
        arg.code = &Interpreter::contIntNumber;
        arg.argIntNumber = node;
        programStack.push(arg);
    }
    virtual void process(ast::LoopExp * node) {
        TrampolineData arg;
        arg.command = Command::Call;
        arg.code = &Interpreter::contLoop;
        arg.argLoopExp = node;
        programStack.push(arg);
    }
    virtual void process(ast::RealNumber * node) {
        TrampolineData arg;
        arg.command = Command::Call;
        arg.code = &Interpreter::contRealNumber;
        arg.argRealNumber = node;
        programStack.push(arg);
    }
    virtual void process(ast::Sequence * node) {
        TrampolineData arg;
        arg.command = Command::Call;
        arg.code = &Interpreter::contSequence;
        arg.argSequence = node;
        programStack.push(arg);
    }
    virtual void process(ast::StringLit * node) {
        TrampolineData arg;
        arg.command = Command::Call;
        arg.code = &Interpreter::contStringLit;
        arg.argStringLit = node;
        programStack.push(arg);
    }
    virtual void process(ast::LenCall * node) {
        TrampolineData arg;
        arg.command = Command::Call;
        arg.code = &Interpreter::contLenCall;
        arg.argLenCall = node;
        programStack.push(arg);
    }
    virtual void process(ast::LoadCall * node) {
        TrampolineData arg;
        arg.command = Command::Call;
        arg.code = &Interpreter::contLoadCall;
        arg.argLoadCall = node;
        programStack.push(arg);
    }
    virtual void process(ast::TypeCall * node) {
        TrampolineData arg;
        arg.command = Command::Call;
        arg.code = &Interpreter::contTypeCall;
        arg.argTypeCall = node;
        programStack.push(arg);
    }

private:
    /* Continuation passing structures and functions */
    enum class Command {
        Call,
        Push,
        Pop
    };    

    struct TrampolineData;
    typedef void(Interpreter::*ContCode)(TrampolineData&);

    struct TrampolineData {
        ContCode code;
        Command command;
        union {
            ast::Expression *argExpression;
            ast::Sequence *argSequence;
            ast::Assignment *argAssignment;
            ast::BinaryExp *argBinaryExp;
            ast::Call *argCall;
            ast::ArrayCall *argArrayCall;
            ast::ConditionalExp *argConditionalExp;
            ast::Identifier *argIdentifier;
            ast::Function *argFunction;
            ast::Index *argIndex;
            ast::IndexedAssignment *argIndexAssignment;
            ast::LoopExp *argLoopExp;
            ast::IntNumber *argIntNumber;
            ast::RealNumber *argRealNumber;
            ast::StringLit *argStringLit;
            ast::LenCall *argLenCall;
            ast::LoadCall *argLoadCall;
            ast::TypeCall *argTypeCall;
        };
    };

    /* Predefined data for push/pop inst. */
    TrampolineData pushData;
    TrampolineData popData;
    void initPushPopTrampolineData() {
        pushData.command = Command::Push;
        popData.command = Command::Pop;
    }

    /* Instruct trampoline where to jump next. Simulates program stack (for instructions). */
    std::stack<TrampolineData> programStack;

    /* The trampoline */
    void trampoline() {
        while (!programStack.empty()) {
            TrampolineData data = programStack.top();
            programStack.pop();
            switch (data.command) {
            case Command::Call:
                (this->*data.code)(data);
                break;
            case Command::Push: // Pushes current result as a temporary
                pushTemp(result_);
                break;
            case Command::Pop: // Pops a temporary
                popTemp();
                break;
            default:
                break; // not reached
            }
        }
    }

    /* Segments created by splitting original methods to allow CP */

    void contExpression(TrampolineData &data) {
        data.argExpression->processBy(this);
    }

    void contAssignment(TrampolineData &data) {
        TrampolineData arg;
        arg.command = Command::Call;
        arg.argAssignment = data.argAssignment;
        arg.code = &Interpreter::contAssignment2;
        programStack.push(arg);

        // Protect RHS and protect it
        programStack.push(pushData);
        data.argAssignment->rhs->processBy(this);
    }

    // Stack before: top <- RHS ....
    void contAssignment2(TrampolineData &data) {
        ast::Assignment * node = data.argAssignment;
        RValue *ident = alloca_->makeOrGetCharacterVector(node->dest->getName());
        RValue *rhs = popAndRetTemp();
        env_->set(ident, rhs);
        result_ = rhs;
    }

    void contBinaryExp(TrampolineData &data) {
        TrampolineData arg;
        arg.argBinaryExp = data.argBinaryExp;
        arg.command = Command::Call;
        arg.code = &Interpreter::contBinaryExp2;
        programStack.push(arg);

        // Process rhs and protect it
        programStack.push(pushData);
        data.argBinaryExp->rhs->processBy(this);

        // Process lhs and protect it
        programStack.push(pushData);
        data.argBinaryExp->lhs->processBy(this);


    }

    // Stack before: top<-RHS .. LHS ....
    void contBinaryExp2(TrampolineData &data) {
        ast::BinaryExp * node = data.argBinaryExp;
        RValue * rhs = getTempFromTop(0);
        RValue * lhs = getTempFromTop(1);
        switch (node->opCode) {
        case ast::BinaryExp::OpCode::add:
            result_ = addValues(lhs, rhs);
            break;
        case ast::BinaryExp::OpCode::sub:
            result_ = subValues(lhs, rhs);
            break;
        case ast::BinaryExp::OpCode::mul:
            result_ = mulValues(lhs, rhs);
            break;
        case ast::BinaryExp::OpCode::div:
            result_ = divValues(lhs, rhs);
            break;
        case ast::BinaryExp::OpCode::lt:
            result_ = ltValues(lhs, rhs);
            break;
        case ast::BinaryExp::OpCode::gt:
            result_ = gtValues(lhs, rhs);
        case ast::BinaryExp::OpCode::neq:
            result_ = neqValues(lhs, rhs);
            break;
        case ast::BinaryExp::OpCode::eq:
            result_ = eqValues(lhs, rhs);
            break;
        default:
            throw InterpreterException("BinOp Not implemented");
        }
        popTemp(); popTemp();
    }

    void contCall(TrampolineData &data) {
        TrampolineData arg;
        arg.argCall = data.argCall;
        arg.command = Command::Call;
        arg.code = &Interpreter::contCall2;
        programStack.push(arg);


        for (ast::Expression *exp : data.argCall->args) {
            // Process argument and protect it
            programStack.push(pushData);
            exp->processBy(this);
        }

        // Process callee and protect it
        programStack.push(pushData);
        data.argCall->callee->processBy(this);
    }

    // Stack before:  top<-arg[0] .. arg[1] ....... arg[n - 1] ... callee ......
    void contCall2(TrampolineData &data) {
        // Lookup the callee, but keep it protected
        RValue * func = getTempFromTop(data.argCall->args.size());
        if (func->type != RValueType::FunctionDef)
            throw InterpreterException("Only functions may be called");
        FunctionDefinition & fdef = func->funcDef; // Reference must be taken !!!
        if (fdef.argCount != data.argCall->args.size())
            throw InterpreterException("Incorrect number of arguments passed");

        if (data.argCall->isTailExpression &&
                env_->getParent() == fdef.envHolder->environment.env) { // Safe but not full
            // Forget everything in this env
            env_->clear();
            // Make bindings from actual arguments to formal parameters in the reused env
            size_t argSize = data.argCall->args.size();
            for (size_t i = 0; i < argSize; i++) {
                env_->set(*(fdef.formalArgs + i), popAndRetTemp());
            }

            popTemp();
            fdef.code->body->processBy(this);
        } else {
            RValue * localEnvHolder = alloca_->makeLocalEnvironment(fdef.envHolder);

            // Make bindings from actual arguments to formal parameters in the new env
            size_t argSize = data.argCall->args.size();
            Environment *local = localEnvHolder->environment.env;
            for (size_t i = 0; i < argSize; i++) {
                local->set(*(fdef.formalArgs + i), popAndRetTemp());
            }

            // Save old env and change into the new one
            pushTemp(envHolder_);
            changeEnvironment(localEnvHolder);

            // Evaluate the function body. Then restore the old environment.
            TrampolineData arg2;
            arg2.argCall = data.argCall;
            arg2.command = Command::Call;
            arg2.code = &Interpreter::contCall3;
            programStack.push(arg2);

            fdef.code->body->processBy(this);
        }
    }

    // Stack before: top<-savedEnvHodler ... callee ...
    void contCall3(TrampolineData &data) {
        // Change back to original env
        RValue  * old = popAndRetTemp();
        changeEnvironment(old);
        // Release the callee
        popTemp();
    }

    void contArrayCall(TrampolineData &data) {
        TrampolineData arg;
        arg.argArrayCall = data.argArrayCall;
        arg.command = Command::Call;
        arg.code = &Interpreter::contArrayCall2;
        programStack.push(arg);

        for (ast::Expression *exp : data.argArrayCall->args) {
            // Evaluate the arg and protect it
            programStack.push(pushData);
            exp->processBy(this);
        }
    }

    // stack before: top<- [0] ... [1] ... [2] ... [3] ...
    void contArrayCall2(TrampolineData &data) {
        // Collect args
        std::vector<RValue*> evalArgs(data.argArrayCall->args.size());
        for (size_t i = 0; i < data.argArrayCall->args.size(); i++) {
            evalArgs[i] = getTempFromTop(i);
        }
        result_ = makeArray(evalArgs);
        // Release them
        for (size_t i = 0; i < evalArgs.size(); i++)
            popTemp();
    }

    void contConditionalExp(TrampolineData &data) {
        TrampolineData arg;
        arg.argConditionalExp = data.argConditionalExp;
        arg.command = Command::Call;
        arg.code = &Interpreter::contConditionalExp2;
        programStack.push(arg);

        // Evaluate condition and protect it
        programStack.push(pushData);
        data.argConditionalExp->cond->processBy(this);
    }

    // Stack before: top<-cond ......
    void contConditionalExp2(TrampolineData &data) {
        if (toBoolean(popAndRetTemp())) {
            // Pass down tail statement mark
            if (data.argConditionalExp->trueCase->isTailExpression)
                data.argConditionalExp->trueCase->isTailExpression = true;
            data.argConditionalExp->trueCase->processBy(this);
        }
        else {
            // Pass down tail statement mark
            if (data.argConditionalExp->falseCase->isTailExpression)
                data.argConditionalExp->falseCase->isTailExpression = true;
            data.argConditionalExp->falseCase->processBy(this);
        }
    }


    void contIndex(TrampolineData &data) {
        TrampolineData arg;
        arg.argIndex = data.argIndex;
        arg.command = Command::Call;
        arg.code = &Interpreter::contIndex2;
        programStack.push(arg);

        // Process index and identifier a protect them both
        programStack.push(pushData);
        data.argIndex->identifier->processBy(this);
        programStack.push(pushData);
        data.argIndex->index->processBy(this);
    }

    // Stack before: top<-target .. index .......
    void contIndex2(TrampolineData &data) {
        RValue * target = getTempFromTop(0);
        RValue * index = getTempFromTop(1);
        result_ = getElement(target, index);
        popTemp(); popTemp();
    }

    void contIndexAssignment(TrampolineData &data) {
        // Store the targetID as well if it is an identifier, in the case we would need to change the binding when performing
        // copy on write
        ast::Identifier *targetIDNode = dynamic_cast<ast::Identifier*>(data.argIndexAssignment->dest->identifier);
        RValue *targetID = nullptr;
        if (targetIDNode != nullptr)
            targetID = alloca_->makeOrGetCharacterVector(targetIDNode->getName());
        pushTemp(targetID);

        TrampolineData arg;
        arg.argIndexAssignment = data.argIndexAssignment;
        arg.command = Command::Call;
        arg.code = &Interpreter::contIndexAssignment2;
        programStack.push(arg);

        programStack.push(pushData);
        data.argIndexAssignment->rhs->processBy(this);
        programStack.push(pushData);
        data.argIndexAssignment->dest->identifier->processBy(this);
        programStack.push(pushData);
        data.argIndexAssignment->dest->index->processBy(this);
    }

    // Stack before: top<-RHS .. target .. index .. targetID
    void contIndexAssignment2(TrampolineData &data) {
        RValue * rhs = getTempFromTop(0);
        RValue * target = getTempFromTop(1);
        RValue * index = getTempFromTop(2);
        RValue * targetID = getTempFromTop(3);
        indexAssignment(target, index, rhs, targetID);
        result_ = rhs;
        popTemp(); popTemp(); popTemp(); popTemp();
    }

    void contLoop(TrampolineData &data) {
        TrampolineData arg;
        arg.argLoopExp = data.argLoopExp;
        arg.command = Command::Call;
        arg.code = &Interpreter::contLoop2;
        programStack.push(arg);

        // Process the condition and store it
        programStack.push(pushData);
        data.argLoopExp->cond->processBy(this);
    }

    // Stack before: top<-condition ....
    void contLoop2(TrampolineData &data) {
        if (toBoolean(popAndRetTemp())) {
            TrampolineData arg;
            arg.argLoopExp = data.argLoopExp;
            arg.command = Command::Call;
            arg.code = &Interpreter::contLoop;
            programStack.push(arg);

            data.argLoopExp->body->processBy(this);
        }
    }

    void contSequence(TrampolineData &data) {
        ast::Sequence *seq = data.argSequence;
        // Pass down tail statement mark to its last statement
        if (!seq->body.empty())
            seq->body.back()->isTailExpression = true;
        else
            result_ = alloca_->makeIntegerVector(0);
        for (int i = seq->body.size() - 1; i >= 0; i--) { // Don't forget the correct order
            seq->body[i]->processBy(this);
        }
    }

    void contIntNumber(TrampolineData &data) {
        result_ = alloca_->makeIntegerVector(data.argIntNumber->value);
    }

    void contRealNumber(TrampolineData &data) {
        result_ = alloca_->makeDoubleVector(data.argRealNumber->value);
    }

    void contStringLit(TrampolineData &data) {
        result_ = alloca_->makeOrGetCharacterVector(data.argStringLit->getName());
    }

    void contFunction(TrampolineData &data) {
        TrampolineData arg;
        arg.command = Command::Call;
        arg.code = &Interpreter::contFunction2;
        arg.argFunction = data.argFunction;
        programStack.push(arg);

        // Create runtime string for each formal parameter and protect it
        for (ast::Identifier * exp : data.argFunction->args) {
            result_ = alloca_->makeOrGetCharacterVector(exp->getName());
            temporaries.push_back(result_);
        }
    }
    // Stack before: top<- [n - 1] ... [n - 2] ... [0] ......
    void contFunction2(TrampolineData &data) {
        size_t argSize = data.argFunction->args.size();
        result_ = alloca_->makeFunctionDefinition(data.argFunction, envHolder_, argSize);
        data.argFunction->links++; // Set runtime link
        for (int i = argSize - 1; i >= 0; i--) { // Watch out for the order, see contFunction pushing.
            // Collect evaluated args
            result_->funcDef.formalArgs[i] = popAndRetTemp();
        }
        // Mark its last statement as tail
        if (!data.argFunction->body->body.empty())
            data.argFunction->body->body.back()->isTailExpression = true;
    }

    void contIdentifier(TrampolineData &data) {
        result_ = env_->get(alloca_->makeOrGetCharacterVector(data.argIdentifier->getName()));
    }

    void contLenCall(TrampolineData &data) {
        TrampolineData arg;
        arg.command = Command::Call;
        arg.code = &Interpreter::contLenCall2;
        arg.argLenCall = data.argLenCall;
        programStack.push(arg);

        // Eval and protect the argument
        programStack.push(pushData);
        data.argLenCall->arg->processBy(this);
    }

    // Stack before: top<-arg .......
    void contLenCall2(TrampolineData &data) {
        // Release the argument
        result_ = alloca_->makeIntegerVector(length(popAndRetTemp()));
    }

    void contLoadCall(TrampolineData &data) {
        TrampolineData arg;
        arg.command = Command::Call;
        arg.code = &Interpreter::contLoadCall2;
        arg.argLoadCall = data.argLoadCall;
        programStack.push(arg);

        // Eval and procect the argument
        programStack.push(pushData);
        data.argLoadCall->arg->processBy(this);
    }

    // Stack before: top<-arg ....
    void contLoadCall2(TrampolineData &data);

    void contLoadCall3(TrampolineData &data) {
        ast::Function * code = loadStack_.back();
        delete code;
        loadStack_.pop_back();
    }

    void contTypeCall(TrampolineData &data) {
        TrampolineData arg;
        arg.command = Command::Call;
        arg.code = &Interpreter::contTypeCall2;
        arg.argTypeCall = data.argTypeCall;
        programStack.push(arg);

        // Eval and procect the argument
        programStack.push(pushData);
        data.argTypeCall->arg->processBy(this);
    }

    // Stack before: top<-arg .....
    void contTypeCall2(TrampolineData &data) {
        // Release the argument and make result
        result_ = alloca_->makeOrGetCharacterVector(RValue::typeToString(popAndRetTemp()->type));
    }


#endif

};
}
#endif // INTERPRETER_H
